#pragma hdrstop
//#include "main.h"
#include "calculations.h"
#include "shapes.h"
#pragma package(smart_init)
/*!
 * \brief Get all shapes for the calculation
 * \param[in] g Pointer to Graph to visualize measurements and data
 * \param[in] s Shapes for the calculation
 * \param[in] n Name of calculation
 */

Calculation::Calculation(Graph* g, std::vector<Shape *> s, std::string n) : graph(g), name(n)
{
	graph->log->Info("Basic class constructor called");
	std::copy(s.begin(), s.end(), std::back_inserter(shapes));
	size_t sz=shapes.size();
	for(size_t i=0; i < sz; i++)
	{
		s.at(i)->myCalcs.push_back(this);
	}
	graph->log->Info("Basic class constructor completed");
}
//------------------------------------------------------------------------------
/*!
 * \brief Default virtual destructor is called by child class destructor
 *
 * Delete this calculation from myCalc vectors of shapes within the calculation
 */
Calculation::~Calculation()
{
	graph->log->Info("Basic class destructor called");
	size_t sz=shapes.size();
	for(size_t i=0; i < sz; i++)
	{
		shapes.at(i)->myCalcs.erase
		(
			find(shapes.at(i)->myCalcs.begin(),shapes.at(i)->myCalcs.end(),this)
		);
	}
	graph->log->Info("Basic class destructor completed");
}
//------------------------------------------------------------------------------
/*!
 * \brief get names of shapes in calculation
 * \param[out] names vector of participating shapes
 */
void Calculation::GetShapes(std::vector <Shape*> * s)
{
	*s = shapes;
}
//------------------------------------------------------------------------------
/*!
 * \brief get the shapes in calculation
 * \param[out] shapes vector of shapes
 */
void Calculation::GetNames(std::vector <std::string> * names)
{
	size_t sz=shapes.size();
	for(size_t i=0; i < sz; i++)
	{
		names->push_back(shapes.at(i)->GetFullName());
	}
}
//------------------------------------------------------------------------------
/*!
 * \brief Highlight all the shapes in the calculation
 */
void Calculation::MakeSelection(bool AuxVisible,bool MarksVisible)
{
	size_t sz=shapes.size();
	for(size_t i=0; i < sz; i++)
	{
		shapes.at(i)->MakeSelection(false);
	}
	if(AuxVisible)
	{
		ShowAux();
		if(MarksVisible)
		{
			ShowMarks();
		}
	}
}
//------------------------------------------------------------------------------
/*!
 * \brief Switch off the highlight at all the shapes in the calculation
 */
void Calculation::RemoveSelection()
{
 	size_t sz=shapes.size();
	for(size_t i=0; i < sz; i++)
	{
		shapes.at(i)->RemoveSelection(false);
	}
	HideAux();
}
//------------------------------------------------------------------------------
/*!
 * \brief Calculate destination point and length of a perpendicular from
 * the pOrigin to segment (pLine1,pLine2)
 */
double Calculation::CalcPerpendicular(Point2D pLine1, Point2D pLine2, Point2D pOrigin, Point2D * pDest)
{
	graph->log->SetMethodName("CalcPerpendicular");
	graph->log->Info("CalcPerpendicular called: \
	pLine1("+FloatToStrF(pLine1.x,ffGeneral,7,3)+", "+FloatToStrF(pLine1.y,ffGeneral,7,3)+"), \
	pLine2("+FloatToStrF(pLine2.x,ffGeneral,7,3)+", "+FloatToStrF(pLine2.y,ffGeneral,7,3)+"), \
	pOrigin("+FloatToStrF(pOrigin.x,ffGeneral,7,3)+","+FloatToStrF(pOrigin.y,ffGeneral,7,3)+")");
	double length=0;
	Point2D pdest;
	double x1,x2,x3,y1,y2,y3;
	x1 = pLine1.x;
	x2 = pLine2.x;
	x3 = pOrigin.x;
	y1 = pLine1.y;
	y2 = pLine2.y;
	y3 = pOrigin.y;
	try
	{
		pdest.x=((x2-x1)*(y2-y1)*(y3-y1)+x1*pow(y2-y1, 2)+x3*pow(x2-x1, 2))/(pow(y2-y1, 2)+pow(x2-x1, 2));
		pdest.y=(y2-y1)*(pdest.x-x1)/(x2-x1)+y1;
		length=sqrt(pow((x3-pdest.x),2)+pow((y3-pdest.y),2));
		*pDest = pdest;
		graph->log->Info("Calculated:  \
		pDest("+FloatToStrF(pdest.x,ffGeneral,7,3)+", "+FloatToStrF(pdest.y,ffGeneral,7,3)+"), \
		length = "+FloatToStrF(length,ffGeneral,7,3));
	}
	catch (Exception &e)
	{
		graph->log->Error(e.Message);
		graph->log->SetMethodName("");
		length = 0;
        *pDest = pdest;
		throw;
	}
	graph->log->SetMethodName("");
	return length;
}
//------------------------------------------------------------------------------
/*!
 * \brief Calculate the length of line segment (p1,p2)
 */
double Calculation::CalcLength(Point2D p1, Point2D p2)
{
	double length=0;
	graph->log->SetMethodName("CalcLength");
	graph->log->Info("CalcLength called: \
	p1("+FloatToStrF(p1.x,ffGeneral,7,3)+", "+FloatToStrF(p1.y,ffGeneral,7,3)+"), \
	p2("+FloatToStrF(p2.x,ffGeneral,7,3)+","+FloatToStrF(p2.y,ffGeneral,7,3)+")");
	try
	{
	   length =	sqrt(pow((p1.x-p2.x),2)+pow((p1.y-p2.y),2));
	   graph->log->Info("length = "+FloatToStrF(length,ffGeneral,7,3));
	}
	catch (Exception &e)
	{
		//todo:process exception
		graph->log->Error(e.Message);
		graph->log->SetMethodName("");
		length = 0;
		throw;
	}
	graph->log->SetMethodName("");
	return length;
}
//------------------------------------------------------------------------------
/*!
 * \brief Calculate the equation and length of line segment (p1,p2)
 */
double Calculation::CalcLineEquation(Point2D p1, Point2D p2, double * K, double * B)
{
	double length=0;
	double k;
	double b;
	try
	{
	   length =	sqrt(pow((p1.x-p2.x),2)+pow((p1.y-p2.y),2));
	   k = (p1.y-p2.y)/(p1.x-p2.x);
	   b = p2.y-k*p2.x;
	   *K = k;
	   *B = b;
	}
	catch (Exception &e)
	{
		//todo:process exception
		length = 0;
		throw;
	}
	return length;
}
//------------------------------------------------------------------------------
/*!
 * \brief Calculate the equation and length of line segment (p1,p2)
 */
Point2D Calculation::CalcCrossPoint(Point2D pL1_1, Point2D pL1_2,Point2D pL2_1, Point2D pL2_2)
{
	Point2D pCross;
	try
	{
			pCross.x=
				-((pL1_1.x*pL1_2.y-pL1_2.x*pL1_1.y)*
				(pL2_2.x-pL2_1.x)-
				(pL2_1.x*pL2_2.y-pL2_2.x*pL2_1.y)*(pL1_2.x-pL1_1.x))/
				((pL1_1.y-pL1_2.y)*(pL2_2.x-pL2_1.x)-
				(pL2_1.y-pL2_2.y)*(pL1_2.x-pL1_1.x));

			pCross.y=
				((pL2_1.y-pL2_2.y)*(-pCross.x)-
				(pL2_1.x*pL2_2.y-pL2_2.x*pL2_1.y))/
				(pL2_2.x-pL2_1.x);
	}
	catch (Exception &e)
	{
		//todo:process exception
		pCross.x = 0;
		pCross.y = 0;
		throw;
	}
	return pCross;
}
//------------------------------------------------------------------------------
/*!
 * \brief Show all auxilary lines
 */
void Calculation::ShowAux()
{
	vector<TLineSeries*>::iterator it;
	for(it=series.begin();it!=series.end();it++)
	{
		(*it)->Visible=true;
	}
}
//------------------------------------------------------------------------------
/*!
 * \brief Hide all auxilary lines
 */
void Calculation::HideAux()
{
	vector<TLineSeries*>::iterator it;
	for(it=series.begin();it!=series.end();it++)
	{
		(*it)->Visible=false;
	}
}
//------------------------------------------------------------------------------
/*!
 * \brief Show marks at all auxilary lines
 */
void Calculation::ShowMarks()
{
	vector<TLineSeries*>::iterator it;
	for(it=series.begin();it!=series.end();it++)
	{
		(*it)->Marks->Visible=true;
	}
}
//------------------------------------------------------------------------------
/*!
 * \brief Hide marks at all auxilary lines
 */
void Calculation::HideMarks()
{
	vector<TLineSeries*>::iterator it;
	for(it=series.begin();it!=series.end();it++)
	{
		(*it)->Marks->Visible=false;
	}
}
//------------------------------------------------------------------------------
//------------------------Specific calculations---------------------------------
//------------------------------------------------------------------------------
AngleBetweenLines::AngleBetweenLines(Graph* g, std::vector<Shape *> s)
		: Calculation(g,s,"Sharp angle")
{
	TCustomChart * chart = g->GetContext();
	chart->Axes->Bottom->LabelStyle=talValue;
	_sL1 = new TLineSeries(chart);
	_sL1->ParentChart =chart;
	_sL1->Pen->Style = psDash;
	_sL1->SeriesColor = clBlack;
	_sL1->Marks->Frame->Color=0x737373;
	_sL1->Marks->Gradient->Visible=true;
	_sL1->Marks->Gradient->Colors->operator [](0)->Color=0xfff8f0;
	_sL1->Marks->Gradient->Direction = gdTopBottom;
	_sL1->Marks->Callout->Length = 10;            //distance
	_sL1->Marks->Callout->ArrowHead = 2;          //triangle style of the head
	_sL1->Marks->Callout->Arrow->Color = 0x737373;
	_sL1->Marks->Callout->Arrow->Visible = true;
    series.push_back(_sL1);

	_sL2 = new TLineSeries(chart);
	_sL2->ParentChart =chart;
	_sL2->Pen->Style = psDash;
	_sL2->SeriesColor = clBlack;
	_sL2->Marks->Frame->Color=0x737373;
	_sL2->Marks->Gradient->Visible=true;
	_sL2->Marks->Gradient->Colors->operator [](0)->Color=0xfff8f0;
	_sL2->Marks->Gradient->Direction = gdTopBottom;
	_sL2->Marks->Callout->Length = 10;            //distance
	_sL2->Marks->Callout->ArrowHead = 2;          //triangle style of the head
	_sL2->Marks->Callout->Arrow->Color = 0x737373;
	_sL2->Marks->Callout->Arrow->Visible = true;
	series.push_back(_sL2);

	HideAux();
	HideMarks();
	g->notify(GRAPH_EVENT::CREATED, this);
}
//------------------------------------------------------------------------------
AngleBetweenLines::~AngleBetweenLines()
{
	_sL1->Clear();
	_sL2->Clear();
	delete _sL1;
	delete _sL2;
}
//------------------------------------------------------------------------------
/*!
 * \brief Calculate angle between lines in degrees
 * \return false if count of shapes is wrong, otherwise true
 */
bool AngleBetweenLines::Calculate()
{
	for(unsigned int i=0; i < series.size(); i++)
	{
		series.at(i)->Clear();
	}

	if(GetSize() != _count_shapes)
	{
		return false;
	}
	double k[_count_shapes];
	double b[_count_shapes];

	for(size_t num = 0; num < shapes.size(); ++num)
	{
		std::vector<double> results;
		std::string name = shapes[num]->GetResults(results);
		if(name == "line")
		{
			k[num] = results[0];
			b[num] = results[1];
		}
	}

	double angle = (k[1] - k[0]) / (1 + k[1] * k[0]);
	double atn = atan(angle);

	_sL1->AddXY(0,b[0],"Line 1 x0,y0");  						//y0=k0x0+b0,x0=0
	_sL1->AddXY(800,k[0]*800+b[0],"Line 1 x1,y1");             	//y1=k0x1+b0,x1=800(matrix width)
	_sL2->AddXY(0,b[1],"Line 2 x0,y0");  					  	//y0=k0x0+b0,x0=0
	_sL2->AddXY(800,k[1]*800+b[1],"Line 2 x1,y1");             	//y1=k0x1+b0,x1=800(matrix width)

	result = atn * 180.0 / M_PI;
	graph->notify(GRAPH_EVENT::UPDATE, this);	//todo:move this call outside for synchronization with form
	return true;
}
//------------------------------------------------------------------------------
DistanceBetweenCenters::DistanceBetweenCenters(Graph* g, std::vector<Shape *> s)
		: Calculation(g, s,"Distance btw centers")
{
	TCustomChart * chart = g->GetContext();
	chart->Axes->Bottom->LabelStyle=talValue;
	_sDist = new TLineSeries(chart);
	_sDist->ParentChart =chart;
	_sDist->Pen->Style = psDash;
	_sDist->SeriesColor = clBlack;
	_sDist->Marks->Visible=true;
	_sDist->Marks->Frame->Color=0x737373;
	_sDist->Marks->Gradient->Visible=true;
	_sDist->Marks->Gradient->Colors->operator [](0)->Color=0xfff8f0;
	_sDist->Marks->Gradient->Direction = gdTopBottom;
	_sDist->Marks->Callout->Length = 10;            //distance
	_sDist->Marks->Callout->ArrowHead = 2;          //triangle style of the head
	_sDist->Marks->Callout->Arrow->Color = 0x737373;
	_sDist->Marks->Callout->Arrow->Visible = true;
	series.push_back(_sDist);

	HideAux();
	HideMarks();
	g->notify(GRAPH_EVENT::CREATED, this);
}
//------------------------------------------------------------------------------
DistanceBetweenCenters::~DistanceBetweenCenters()
{
	_sDist->Clear();
	delete _sDist;
}
//------------------------------------------------------------------------------
/*!
 * \brief Calculate the distance between centers of circles
 * \return false if count of shapes is wrong, otherwise true
 */
bool DistanceBetweenCenters::Calculate()
{
	for(unsigned int i=0; i < series.size(); i++)
	{
		series.at(i)->Clear();
	}

	if(GetSize() != _count_shapes)
	{
		return false;
	}
	double x[_count_shapes];
	double y[_count_shapes];

	for(size_t num = 0; num < shapes.size(); ++num)
	{
		std::vector<double> results;
		std::string name = shapes[num]->GetResults(results);
		if(name == "circle")	//todo:change to index!
		{
			x[num] = results[0];
			y[num] = results[1];
		}
		else if(name == "ellipse")
		{
			x[num] = results[0];
			y[num] = results[1];
		}
		else if(name == "line")
		{
			x[num] = (results[5]+results[7])/2;
			y[num] = (results[6]+results[8])/2;
		}
	}

	_sDist->AddXY(x[0],y[0],"x0,y0");
	_sDist->AddXY(x[1],y[1],"x1,y1");

	result = sqrt(pow((x[1]-x[0]),2)+pow((y[1]-y[0]),2));
	graph->notify(GRAPH_EVENT::UPDATE, this);	//todo:move this call outside for synchronization with form
	return true;
}
//------------------------------------------------------------------------------
/*!
 * \brief Constructor creates all chart series for visualizing auxilary calculations
 *  and notifies the observer
 * \param[in] g Pointer to Graph
 * \param[in] s Link to the vector vith assigned shapes
 */
DistanceBetweenLines::DistanceBetweenLines(Graph *g, std::vector<Shape *> s)
			: Calculation (g, s, "Distance btw lines")
{
	TCustomChart * chart = g->GetContext();
	chart->Axes->Bottom->LabelStyle=talValue;
	sB = new TLineSeries(chart);
	sL1= new TLineSeries(chart);
	sL2= new TLineSeries(chart);
	for(int i=0; i<6; i++)
	{
		sp[i] = new TLineSeries(chart);
		sp[i]->ParentChart = chart;
		sp[i]->Marks->Visible=true;
		sp[i]->Marks->Frame->Color=0x737373;
		sp[i]->Marks->Gradient->Visible=true;
		sp[i]->Marks->Gradient->Colors->operator [](0)->Color=0xfff8f0;
		sp[i]->Marks->Gradient->Direction = gdTopBottom;
		sp[i]->Marks->Callout->Length = 10;            //distance
		sp[i]->Marks->Callout->ArrowHead = 2;          //triangle style of the head
		sp[i]->Marks->Callout->Arrow->Color = 0x737373;
		sp[i]->Marks->Callout->Arrow->Visible = true;
		series.push_back(sp[i]);
//sp[i]->Marks->Callout->Style-
	}
	sB->ParentChart = chart;
	sL1->ParentChart = chart;
	sL2->ParentChart = chart;

	sL1->Pen->Style = psDash;
	sL2->Pen->Style = psDash;
	sB->Pen->Style = psDashDot;
	sB->Color = clGreen;

	sB->Marks->Frame->Color=0x737373;
	sB->Marks->Gradient->Visible=true;
	sB->Marks->Gradient->Colors->operator [](0)->Color=0xfff8f0;
	sB->Marks->Gradient->Direction = gdTopBottom;
	sB->Marks->Callout->Length = 10;            //distance
	sB->Marks->Callout->ArrowHead = 2;          //triangle style of the head
	sB->Marks->Callout->Arrow->Color = 0x737373;
	sB->Marks->Callout->Arrow->Visible = true;

	sL1->SeriesColor = clBlack;
	sL1->Marks->Frame->Color=0x737373;
	sL1->Marks->Gradient->Visible=true;
	sL1->Marks->Gradient->Colors->operator [](0)->Color=0xfff8f0;
	sL1->Marks->Gradient->Direction = gdTopBottom;
	sL1->Marks->Callout->Length = 10;            //distance
	sL1->Marks->Callout->ArrowHead = 2;          //triangle style of the head
	sL1->Marks->Callout->Arrow->Color = 0x737373;
	sL1->Marks->Callout->Arrow->Visible = true;

	sL2->SeriesColor = clBlack;
	sL2->Marks->Frame->Color=0x737373;
	sL2->Marks->Gradient->Visible=true;
	sL2->Marks->Gradient->Colors->operator [](0)->Color=0xfff8f0;
	sL2->Marks->Gradient->Direction = gdTopBottom;
	sL2->Marks->Callout->Length = 10;            //distance
	sL2->Marks->Callout->ArrowHead = 2;          //triangle style of the head
	sL2->Marks->Callout->Arrow->Color = 0x737373;
	sL2->Marks->Callout->Arrow->Visible = true;

	series.push_back(sB);
	series.push_back(sL1);
	series.push_back(sL2);

	HideAux();
	HideMarks();
	g->notify(GRAPH_EVENT::CREATED, this);
}
//------------------------------------------------------------------------------
/*!
 * \brief Destructor deletes all auxilary calculations
 */
DistanceBetweenLines::~DistanceBetweenLines()
{
	sB->Clear();
	sL1->Clear();
	sL2->Clear();
	for(int i=0; i<6; i++)
	{
		sp[i]->Clear();
	}

	delete sB;
	delete sL1;
	delete sL2;
	for(int i=0; i<6; i++)
	{
		delete sp[i];
	}
}
//------------------------------------------------------------------------------
/*!
 * \brief Calculate the distance between lines by bisector
 * \return false if count of shapes is wrong, otherwise true
 *
 * The algorithm is following:
 * 1. Calculate the equations of line segments
 * 2. If lines are parallel, calculate perpendicular from one line to another one, it's length is result. Return.
 * 3. Calculate the point of intersection of assigned lines. If line segments are crossing, result is 0. Return.
 * 4. Calculate the bisection
 * 5. Put perpendiculars to the bisection from vertexes of line segments
 * 6. Check if the lines are overlapped. If not, result is the sum of the nearest perpendiculars,
 *    if yes - result is sum of perpendiculars from the center of overlapping region at the bisector
 *    to the lines.
 */
bool DistanceBetweenLines::Calculate()
{
	double k[_count_shapes];        //k coeffs of assigned lines
	double b[_count_shapes];        //b coeffs of assigned lines
	double angleX[2];               //horizontal angles of assigned lines
	Point2D pShape[_count_shapes*2];//vertexes of line segments
	Point2D pCross;					//point of crossing the lines
	Point2D pBisect[3];				//points that belong to the bisector
	Point2D pPerpBisect[4];			//points of the perpendiculars at the bisector
	Point2D pPerpFromOLCenter[2];	//points of the perpendiculars from the overlapping center
	Point2D pPerpLine[2];			//points of the perpendiculars at the lines
	Point2D pOLCenter;				//the center of overlapping region (at the bisector)
	double Lp[6]={0}; 				//lengths of the perpendiculars
	double Kb;						//K of bisector
	double Bb;                      //B of bisector
	double Lb;                      //length of bisector

	for(unsigned int i=0; i < series.size(); i++)
	{
		series.at(i)->Clear();
	}

	if(GetSize() != _count_shapes)
	{
		return false;
	}

	unsigned int pIdx=0;		 	//index of the vertex
	for(size_t num = 0; num < shapes.size(); ++num)
	{
		std::vector<double> results;
		std::string name = shapes[num]->GetResults(results);
		if(name == "line")
		{
			k[num] = results[0];
			b[num] = results[1];
			angleX[num] = results[3];
			pShape[pIdx].SetX(results[5]);
			pShape[pIdx].SetY(results[6]);
			pIdx++;
			pShape[pIdx].SetX(results[7]);
			pShape[pIdx].SetY(results[8]);
			pIdx++;
		}
	}

	Point2D pLine1[2];					//vertexes of fully stretched assigned line �1
	Point2D pLine2[2];					//vertexes of fully stretched assigned line �2
	pLine1[0].x=0, pLine2[0].x=0;   	//x0 for line 1, x0 for line 2
	pLine1[1].x=800, pLine2[1].x=800;	//x1 for line 1, x1 for line 2 - X size of laser matrix

	pLine1[0].y=k[0]*pLine1[0].x+b[0];
	pLine2[0].y=k[1]*pLine2[0].x+b[1];
	pLine1[1].y=k[0]*pLine1[1].x+b[0];
	pLine2[1].y=k[1]*pLine2[1].x+b[1];

	//draw assigned line 1
	sL1->AddXY(pLine1[0].x,pLine1[0].y,"Line1 x0,y0");
	sL1->AddXY(pLine1[1].x,pLine1[1].y,"Line1 x1,y1");

	//draw assigned line 2
	sL2->AddXY(pLine2[0].x,pLine2[0].y,"Line2 x0,y1");
	sL2->AddXY(pLine2[1].x,pLine2[1].y,"Line2 x1,y1");
	double la,lb,lc,x1,x2,x3,y1,y2,y3;

	try
	{
		if(k[0]==k[1])    //if lines are parallel
		{
			//result is the length of the perpendicular from one vertex to another line
			result = CalcPerpendicular(pShape[2],pShape[3],pShape[0],&pPerpBisect[0]);

			//show auxilary calculations
			//draw bisector
			sB = new TLineSeries(graph->GetContext());
			sB->ParentChart = graph->GetContext();
			sB->AddXY(pBisect[1].x,pBisect[1].y);
			sB->AddXY(pBisect[2].x,pBisect[2].y);

			//perpendicular
			sp[0]->AddXY(pShape[0].x,pShape[0].y,"Shape "+IntToStr(0));
			sp[0]->AddXY(pPerpBisect[0].x,pPerpBisect[0].y,"Perp"+IntToStr(0));


			OutputDebugString(L"line segments are parallel");
		}
		else
		{
		//the point of crossing lines
			pCross = CalcCrossPoint(pLine1[0],pLine1[1],pLine2[0],pLine2[1]);
		//check if lines are crossing
			if(
				(pShape[0].x <= pCross.x && pCross.x <= pShape[1].x)&&
				(pShape[2].x <= pCross.x && pCross.x <= pShape[3].x)
			)
			{
				result = 0;
				graph->notify(GRAPH_EVENT::UPDATE, this);
				OutputDebugString(L"line segments are crossing");
				return true;
            }
			x1=pLine1[0].x;
			y1=pLine1[0].y;
			x2=pLine2[0].x;
			y2=pLine2[0].y;
			x3=pCross.x;
			y3=pCross.y;
			lc=CalcLength(pLine1[0],pLine2[0]);
			la=CalcLength(pLine2[0],pCross);
			lb=CalcLength(pCross,pLine1[0]);

			//coordinates of the bisector
			pBisect[0].x=(la*x1+lb*x2+lc*x3)/(la+lb+lc);
			pBisect[0].y=(la*y1+lb*y2+lc*y3)/(la+lb+lc);

			//now calculate the equation of bisector by calculated vertexes
			Kb=(pBisect[0].y-pCross.y)/(pBisect[0].x-pCross.x);
			Bb = pCross.y-Kb*pCross.x;
			Lb = CalcLineEquation(pBisect[0],pCross,&Kb,&Bb);

			double tanb = (Kb) / (1 + Kb);
			double angleb = atan(tanb)*180/M_PI;

			//vertexes of the bisector through the full chart
			pBisect[1].x=0;
			pBisect[2].x=800;
			pBisect[1].y=Kb*pBisect[1].x+Bb;
			pBisect[2].y=Kb*pBisect[2].x+Bb;


			//debug: horisontal bisector, for better view of perpendiculars
			//pBisect[0].y=pBisect[1].y=pBisect[2].y=pCross.y;
			// \debug

			//calculate the coordinates and lengths of the perpendiculars
			for (int i = 0; i < 4; i++)
			{
				Lp[i]=CalcPerpendicular(pBisect[1],pBisect[2],pShape[i],&pPerpBisect[i]);
			}
			//calculate the equation of some perpendicular to get K coefficient for
			//creating parallel line from the overlapping center
			double kP;
			double bP;
			CalcLineEquation(pPerpBisect[0],pShape[0],&kP,&bP);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////FINAL CALCULATIONS//////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	//check if lines are overlapped. There are 3 situations
	// - line segments aren't overlapped,
	// - line segments are partly overlapped,
	// - line segments are completely overlapped

			pPerpFromOLCenter[0].y = 0;
			pPerpFromOLCenter[1].y = 800;	//height of laser matrix

			//check if our lines are drawn left to right
			if((pPerpBisect[0].x < pPerpBisect[1].x) && (pPerpBisect[2].x < pPerpBisect[3].x))
			{
			//if line segments aren't overlapped, first is lefter
				if 	(pPerpBisect[1].x <= pPerpBisect[2].x)
				{
					//result is the sum of lenghts of the nearest perpendiculars
					result = Lp[1]+Lp[2];
					OutputDebugString(L"line segments aren't overlapped, first is lefter");
				}
			//if line segments aren't overlapped, first is righter
				else if (pPerpBisect[3].x <= pPerpBisect[0].x)
				{
					result = Lp[3]+Lp[0];
					OutputDebugString(L"line segments aren't overlapped, first is righter");
				}
				else
				{
				//if line segments are partly overlapped, first line is letfer
					if ((pPerpBisect[1].x > pPerpBisect[2].x) && (pPerpBisect[1].x <= pPerpBisect[3].x) && (pPerpBisect[0].x < pPerpBisect[2].x))
					{
						//calculate the center of overapped region at the bisector
						pOLCenter.x = (pPerpBisect[1].x + pPerpBisect[2].x)/2;
						pOLCenter.y = Kb*pOLCenter.x+Bb;

						OutputDebugString(L"line segments are partly overlapped, first line is letfer");
					}
				//if line segments are partly overlapped, first line is righter
					else if ((pPerpBisect[0].x >= pPerpBisect[2].x)  && (pPerpBisect[0].x < pPerpBisect[3].x) && (pPerpBisect[1].x > pPerpBisect[3].x))
					{
						//calculate the center of overapped region at the bisector
						pOLCenter.x = (pPerpBisect[0].x + pPerpBisect[3].x)/2;
						pOLCenter.y = Kb*pOLCenter.x+Bb;

						OutputDebugString(L"line segments are partly overlapped, first line is righter");
					}
				//if line segments are completely overlapped, first is longer
					else if ((pPerpBisect[0].x <= pPerpBisect[2].x) && (pPerpBisect[1].x >= pPerpBisect[3].x))
					{
						//calculate the center of overapped region at the bisector
						pOLCenter.x = (pPerpBisect[2].x + pPerpBisect[3].x)/2;
						pOLCenter.y = Kb*pOLCenter.x+Bb;
						OutputDebugString(L"line segments are completely overlapped, first line is longer");
					}
				//if line segments are completely overlapped, second is longer
					else if ((pPerpBisect[0].x >= pPerpBisect[2].x) && (pPerpBisect[1].x <= pPerpBisect[3].x))
					{
						//calculate the center of overapped region at the bisector
						pOLCenter.x = (pPerpBisect[0].x + pPerpBisect[1].x)/2;
						pOLCenter.y = Kb*pOLCenter.x+Bb;
						OutputDebugString(L"line segments are completely overlapped, second line is longer");
					}

					//calculate the parallel line for the perpendiculars through the overlapping center
					//K of this line is like K of perpendiculars, we should only calculate B
					double bOLC=pOLCenter.y-kP*pOLCenter.x;

					pPerpFromOLCenter[0].x = (pPerpFromOLCenter[0].y-bOLC)/kP;
					pPerpFromOLCenter[1].x = (pPerpFromOLCenter[1].y-bOLC)/kP;

					//calculate the points where this lines intersect assigned segments
					pPerpLine[0] = CalcCrossPoint(pPerpFromOLCenter[0],pOLCenter,pShape[0],pShape[1]);
					pPerpLine[1] = CalcCrossPoint(pPerpFromOLCenter[0],pOLCenter,pShape[2],pShape[3]);
					result = CalcLength(pPerpLine[0],pPerpLine[1]);

					sp[4]->AddXY(pOLCenter.x,pOLCenter.y,"OL center");
					sp[4]->AddXY(pPerpLine[0].x,pPerpLine[0].y,"PerpL 0");

					sp[5]->AddXY(pOLCenter.x,pOLCenter.y,"OL center");
					sp[5]->AddXY(pPerpLine[1].x,pPerpLine[1].y,"PerpL 1");
				}
			}
			else
			{
	//todo:test this case
				int a=0;
			}
			//show auxilary calculations
			sB->AddXY(pBisect[1].x,pBisect[1].y,"Bisector x0,y0");
			sB->AddXY(pBisect[2].x,pBisect[2].y,"Bisector x0,y0");

			//perpendiculars
			for(int i=0; i<4; i++)
			{
				sp[i]->AddXY(pShape[i].x,pShape[i].y,"Shape "+IntToStr(i));
				sp[i]->AddXY(pPerpBisect[i].x,pPerpBisect[i].y,"PerpB "+IntToStr(i));
			}
		}
		graph->notify(GRAPH_EVENT::UPDATE, this);	//todo:move this call outside for synchronization with form
	}
	catch(...)
	{
		result=0;
	}
	return true;
}
