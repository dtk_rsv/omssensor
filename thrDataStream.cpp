//---------------------------------------------------------------------------
#pragma hdrstop

#include "thrDataStream.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
TIdUDPClient *udp;
TIdTCPClient *tcp;
TDataStream *thrDataStream;
//---------------------------------------------------------------------------
/*!
 * \brief Create the stream of receiving data from a device using callback methods
 *
 * \param[in] CreateSuspended inherited from TThread
 * \param[in] callbackFrame method of updating the frame
 * \param[in] callbackVideo method of updating video
 * \param[in] callbackExc method of cathing exceptions
 */
__fastcall TDataStream::TDataStream (
						bool CreateSuspended,
						CallbackFrame_t callbackFrame,
						CallbackVideo_t callbackVideo,
						CallbackException_t callbackExc
					   ) :
	TThread(CreateSuspended),
	_callbackFrame(callbackFrame),
	_callbackVideo(callbackVideo),
	_callbackExc(callbackExc)
{
	TIniFile * ini = new TIniFile(".\\config.ini");
	_minLogSize = ini->ReadInteger("BufferSize","core",100);
	log = new Log(".\\Log\\Core.log",_minLogSize);
	log->SetClassName(((AnsiString)ClassName()).c_str());
	log->SetMethodName(((AnsiString)ClassName()).c_str());
	log->Info("Constructor called");
	_threadMode = THREADMODE_MEASUREMENT;
	_connected=false;



	log->Info("UDP client creation");
	udp=new TIdUDPClient ();
	udp->Port=ini->ReadInteger("udp","devport",5001);
	udp->Host=ini->ReadString("udp","devip","192.168.1.2");

	udp->BoundPort=ini->ReadInteger("udp","localport",5003);
	udp->BoundPortMin = ini->ReadInteger("udp","localportmin",5000);
	udp->BoundPortMax = ini->ReadInteger("udp","localportmax",5050);
	udp->BufferSize = ini->ReadInteger("udp","buffer_size",8192);
	log->Info("OK");

	log->Info("TCP client creation");
	tcp = new TIdTCPClient ();
	tcp->Port = ini->ReadInteger("tcp","devport",5001);
	tcp->Host = ini->ReadString("tcp","devip","192.168.1.2");
	tcp->BoundPort = ini->ReadInteger("tcp","localport",5003);
	tcp->ConnectTimeout = ini->ReadInteger("tcp","connect_timeout",1000);
	tcp->ReuseSocket=true;	//don't wait for closing socket from server
	_ctrlProtocol = ini->ReadInteger("misc","control_protocol",CP_UDP);
	log->Info("OK");

	_sl = new TStringList;

	_calibrationX = new Calibration();
	_calibrationX->Init(".\\calibrationX.ini");
	_calibrationZ = new Calibration();
	_calibrationZ->Init(".\\calibrationZ.ini");

	_matrixWidth = ini->ReadInteger("MatrixSize","width",800);
	_matrixHeight = ini->ReadInteger("MatrixSize","width",600);

	_nOfBorders = ini->ReadInteger("misc","nOfBorders",1);
	_filterSize = ini->ReadInteger("Misc","filtersize",3);

	_filtrationRequired = ini->ReadInteger("misc","filtration_required",1);

	_parseMethod = ini->ReadInteger("misc","input_format",0);

	log->Info("Filters creation");
	vector<Filter*> tempV;

	for(int i=0; i < _nOfBorders; i++)
	{
		_filters.push_back(tempV);
		for(int j=0; j < _matrixWidth; j++)
		{
			_filters.at(i).push_back(new Filter(_filterSize));
		}

	}

	_count_points_received = 0;
	_parameters.push_back(500.0);			//gradient
	_parameters.push_back(ini->ReadInteger("misc","buffer_size",-1));	//buffer size
	delete ini;
	log->Info("Constructor completed");
	log->SetMethodName("");
}
//---------------------------------------------------------------------------
 /*!
  *	\brief Disconnect network clients
  */
__fastcall TDataStream::~TDataStream()
{
	for(int i=0; i < _nOfBorders; i++)
	{
		for(int j=0; j < _matrixWidth; j++)
		{
			delete (_filters.at(i).at(j));
		}
	}
	_filters.clear();
	Disconnect();
	delete udp;
	delete tcp;
}
//---------------------------------------------------------------------------
 /*!
  *	\brief Request for connecting network clients
  */
void TDataStream::Connect()
{
	log->Info("Requesting for connection");
	_connectState = CS_CONNECT;
}
//---------------------------------------------------------------------------
 /*!
  *	\brief Request for disconnecting network clients
  */
void TDataStream::Disconnect()
{
	log->Info("Requesting for disconnection");
	_connectState = CS_DISCONNECT;
}
//---------------------------------------------------------------------------
 /*!
  *	\brief Connect network clients
  */
void TDataStream::_Connect()
{
	log->SetMinSize(_minLogSize);
	log->SetMethodName("_Connect");
	log->Info("Method called");
	if(_ctrlProtocol==CP_TCP)
	{
		try
		{
			log->Info("Trying to establish TCP connection");
			tcp->Connect();		//try to establish TCP connection
			_connected=true;
			log->Info("Established OK");
		}
		catch(Exception &e)
		{
			log->Error("Couldn't establish the connection");
			_connected=false;
			throw;
		}
	}
	else
	{
	   	try
		{
			log->Info("Trying to establish UDP connection");
//  			udp->Connect();		//try to establish TCP connection
			_connected=true;
		}
		catch(Exception &e)        	//dummy unreachable code
		{
			log->Error("Couldn't establish the connection");
			_connected=false;
			throw;
		}
		_connected = true;		//don't try to connect when using UDP, just accept it
	}
	_connectState = CS_IDLE;
	log->Info("Method completed");
	log->SetMethodName("");
}
 /*!
  *	\brief Disconnect network clients
  */
void TDataStream::_Disconnect()
{
	log->SetMinSize(1);
	log->SetMethodName("_Disonnect");
	if(_connected)
	{
		try
		{
			if(_ctrlProtocol==CP_UDP)
			{
				log->Info("Disconnecting the UDP client");
				_connected=false;
				udp->Disconnect();
			}
			else
			{
				log->Info("Disconnecting the TCP client");
				_connected=false;
				tcp->IOHandler->InputBuffer->Clear();
				tcp->IOHandler->CloseGracefully();
				tcp->Disconnect();
			}
		}
		catch (Exception &e)
		{
			log->Error("Disconnection error" + e.Message);
        }

	}
	_connectState = CS_IDLE;
	log->Info("Method completed");
	log->SetMethodName("");
}
//---------------------------------------------------------------------------
 /*!
  *	\brief The stream of receiving data from device
  */
void __fastcall TDataStream::Execute()
{
	while(!Terminated)
	{
		try
		{
			//log->Info("Checking _connectState: "+IntToStr((int)_connectState));
			switch(_connectState) 									//first check if
			{                                                       //we should connect
				case CS_CONNECT:                                    //or disconnect the network.
					_Connect();                                    	//
					break;                                          //This way provided to
				case CS_DISCONNECT:                                 //synchronize user form with
					_Disconnect();                                	//this thread
					break;
				case CS_IDLE:
					break;
			}
		}
		catch (Exception &e)
		{
			log->Error(e.Message);
 			Disconnect();
			_exception = &e;
			Synchronize(_ShowException);
        }
		if(_connected) try
		{
			while(!_commands.empty())               		 	//check if there are some commands to send
			{
				if(_ctrlProtocol==CP_UDP)
				{
					log->Info("Sending via UDP: " + _commands.front());
					udp->Send(_commands.front(),0);           	//send this commands via udp
				}
				else
				{
					log->Info("Sending via TCP: " + _commands.front());
					tcp->IOHandler->WriteLn(_commands.front()); //or tcp
				}

				_commands.pop();
			}
			log->Info("Command queue is empty");
			switch(_threadMode)
			{
				case THREADMODE_SHOWHYSTOGRAM:
				case THREADMODE_SHOWPOINTS:
				case THREADMODE_SHOWCIRCLES:
					_ReceiveVideoFrame();
					break;
				case THREADMODE_MEASUREMENT:
					_ReceiveMeasurementString();
					break;
				default:
					break;
			}
			SetMode(_threadMode);
		}
		catch(Exception &e)
		{
			log->Error(e.Message);
			Disconnect();
			_exception = &e;
			Synchronize(_ShowException);
        }

	}
	log->Info("Terminated");
	log->SetMethodName("");

}
//---------------------------------------------------------------------------
/*!
 * \brief Receive and process measurement string with coordinates
 */
void TDataStream::_ReceiveMeasurementString()
{
	Point2D point;
	String str;
	unsigned int index;								//index of packet
	unsigned int count;                 			//count of values in the packet
	double x, y;                  					//received x and y values
	unsigned int borderIdx = 0;						//index of border in signal
	unsigned int len;								//length of one symbol (x or y)
	unsigned int lenCoord;
	unsigned int idxStart;
	unsigned int countParams;
	uint16_t g =0;
	int iCount = 0;
	switch (_parseMethod)
	{
		case F_INT:
			len = 3;
			idxStart = 6;
			countParams = 2;
			lenCoord = len*countParams;
			break;
		case F_DEC:
			len = 8;
			idxStart = 6;
			countParams = 2;
			lenCoord = len*countParams;
			break;
		case F_GRAD:
			len = 4;
			idxStart = 9;
			countParams = 3;
			lenCoord = len*3;					//x(4bytes),y(4bytes),g(2bytes)
			break;
	}

	do
	{
  //		OutputDebugString(("Buffer reading cycle " + IntToStr(iCount++)).c_str());
		try
		{
			log->Info("Trying to receive a datagram");
			str=udp->ReceiveString(100,0);  //try to receive a datagram
		}
		catch(Exception &e)
		{
			log->Error(e.Message);
		}
		FormatSettings.DecimalSeparator = '.';
	//rsv:debug
	//	Sleep(100);
	//	str = "0000500a00a00a00b00b00b00b00c00c00c";// \debug
		if (str.Length())
		{
			if(str=="Unkn")
			{
				SetMode(THREADMODE_MEASUREMENT);
				return;
			}

			try
			{
				log->Info("Parsing the datagram");
				if(_parseMethod!=F_GRAD)
				{
					index = ("0x"+str.SubString(1,2)).ToInt();  		//index of packet
				}
				else
				{
					index = ("0x"+str.SubString(1,4)).ToInt();
				}
				if (index == 0)  									//all processing is at the first packet
				{
					if(_exportFrame)    							//check if we should export first fully captured frame
					{
						if(!_slAllowRec)
						{
							_slAllowRec=true;						//start capturing new frame into stringlist
						}
						else
						{
							_SaveFrame();                        	//release and save the frame
						}
					}
					_parameters.at(TP_BUFFERSIZE) == -1 ? _fragmentType = FT_FULL : _fragmentType = FT_FIRST;
					Synchronize(_UpdFrame);							//refresh frame at the first packet
					_xValues.clear();
					_yValues.clear();
					_xyValues.clear();
				}
				if(_parameters.at(TP_BUFFERSIZE) != -1 && _count_points_received >= _parameters.at(TP_BUFFERSIZE))
				{
					_count_points_received = 0;
					_fragmentType = FT_FRAGMENT;
					Synchronize(_UpdFrame);							//refresh frame at the first packet
					_xValues.clear();
					_yValues.clear();
					_xyValues.clear();
				}
				if(_parseMethod!=F_GRAD)
				{
					count =  ("0x"+str.SubString(3,3)).ToInt()*len*2+1;		//count of packets in the datagram
				}
				else
				{
					count =  ("0x"+str.SubString(5,4)).ToInt()*lenCoord+1;		//count of packets in the datagram
				}
				for (unsigned int i = idxStart; i < count; i += lenCoord)
				{

					switch(_parseMethod)
					{
					case F_INT:

						y = ("0x"+str.SubString(i,len)).ToInt();
						x = ("0x"+str.SubString(i+len,len)).ToInt();
						break;
					case F_DEC:

						x = (str.SubString(i,len)).ToDouble();
						y = (str.SubString(i+len,len)).ToDouble();
						break;
					case F_GRAD:
						x = ("0x"+str.SubString(i,len)).ToInt();
						y = ("0x"+str.SubString(i+len,len)).ToInt();
						g = ("0x"+str.SubString(i+len*2,len)).ToInt();
					}
					//todo:process applying data for signals with 2 (or more) borders

					_Convert(&x,&y);					   			//convert values by calibration methods

					if(_filtrationRequired == 1 && !_xValues.empty())
					{

						try
						{
							if(x == _xValues.back())					//border separation
							{
								if(borderIdx+1 < _nOfBorders)
								{
									borderIdx++;
								}
							}
							_filters.at(borderIdx).at(x)->Set(y);      	//applying interframe filtration
							y = _filters.at(borderIdx).at(x)->Get();  	//get filtered value for this X coordinate
						}
						catch(Exception &e)
						{
							log->Error("Filtration error: "+e.Message);
						}
					}

	//				if(_parseMethod == F_GRAD)
	//				{
	//					if(g<_parameters.at(TP_GRADIENT))
	//					{
	////						log->Info("Value "+IntToStr((int)g)+" is less than given gradient level");
	//                     	y = 0;
	//                    }
	//				}
					_count_points_received++;
					point.SetX(x);
					point.SetY(y);
					_xValues.push_back(x);                   		//insert new points into frame
					_yValues.push_back(y);
					_xyValues.push_back(point);

					//if setting data to string list is allowed
					if(_slAllowRec)
					{
						_sl->Add(UIntToStr(index)+"\t"+
			   //				UIntToStr(count)+"\t"+
							FormatDateTime("hh:mm:ss,zzz",Now())+"\t"+
							str.SubString(i,len*countParams)+"\t"+
							FloatToStrF(x,ffGeneral,5,3)+"\t"+
							FloatToStrF(y,ffGeneral,5,3)+"\t"+
							FloatToStrF(g,ffGeneral,5,3));
					}
				}
				borderIdx = 0;										//restart from the first border
			}
			catch (Exception &e)
			{
				_exception = &e;
				log->Error(e.Message);
			}
		}
		else
		{
			log->Warning("Datagram is empty (No connection)");
		}
	} while(str.Length());

}
//---------------------------------------------------------------------------
/*!
 * \brief Receive and process measurement string with coordinates
 */
void  TDataStream::_ReceiveVideoFrame()
{
	String str;
	TIdBytes buf;
	int BytesRead;
	int ImgSize;
	TMemoryStream* memStream;

	try
	{
		log->Info("Trying to receive video frame");
		buf.Length=1024;

		int BytesLeft=ImgSize;

		str=udp->ReceiveString(100,0);
		if (str.SubString(0,2)=="SG")
		{
			str=udp->ReceiveString(50,0);
			if (str.SubString(0,5)=="Start")
			{
				buf.set_length(sizeof(int));
				BytesRead=udp->ReceiveBuffer(buf,-1);
				ImgSize=*(int*)(&buf[0]);
				BytesLeft=ImgSize;
				if (BytesRead==4)
				{
					buf.set_length(32769);
					memStream= new TMemoryStream;
					while(BytesRead)
					{
						BytesRead=udp->ReceiveBuffer(buf,50);
						BytesLeft-=BytesRead;
						if (BytesRead>0) memStream->Write(&buf[0],BytesRead);
					}
					memStream->Position=0;
					_bmp= new Graphics::TBitmap;
					_bmp->LoadFromStream(memStream);
					Synchronize(_UpdVideo);
//					Image1->Canvas->Draw(0,0,_bmp);
//					//delete pix;	//rsv:i think it isn't needed
					delete memStream;
				}
			}
		}
		BytesRead=1;
		buf.set_length(32769);
		while(BytesRead) BytesRead=udp->ReceiveBuffer(buf,100);
	}
	catch (Exception &e)
	{
		log->Error(e.Message);
	}

}
//---------------------------------------------------------------------------
/*!
 * \brief Convert the Y value according with calibration methods
 * \param [in][out] x X value
 * \param [in][out] y Y(X) value
 * \return true if convertion succeeded, otherwise false
 */
bool TDataStream::_Convert(double * x, double * y)
{
	double meanX = _calibrationX->GetNormalizedMean();
	double meanY = _calibrationZ->GetNormalizedMean();
	double stdX = _calibrationX->GetNormalizedStd();
	double stdY = _calibrationZ->GetNormalizedStd();

	double meanZ = _calibrationX->GetNormalizedMeanZ();
	double stdZ = _calibrationX->GetNormalizedStdZ();


	double offsetX = _calibrationX->GetOffset();
	double offsetY = _calibrationZ->GetOffset();
	double reflectionX = _calibrationX->GetReflection();
	double reflectionY = _calibrationZ->GetReflection();
	double y_intermediate;						//intermediate value
	double x_intermediate;
	double cur_x = *x;
	double cur_y = *y;


	// Inversion
	cur_x *= reflectionX;
	cur_y *= reflectionY;

	// Normalizing coefficients
	cur_y -= meanY;
	cur_x -= meanX;

	try
	{
		cur_y /= stdY;
		cur_x /= stdX;
	}
	catch(...)
	{
		log->Error("_Convert: Division by zero (std = 0)");
	}

	// polynomial approximation for Y
	try
	{
		y_intermediate = _calibrationZ->Poly9(cur_y);
		cur_y = *y;
		cur_y *= reflectionY;
		cur_y-=meanZ;
        cur_y/=stdZ;

		x_intermediate = _calibrationX->Matrix(cur_x,cur_y);
	}
	catch(...)
	{
		log->Error("_Convert: polynomial approximation error");
	}

	if(y_intermediate == 0xDEADBEEF||x_intermediate == 0xDEADBEEF)
	{
		return false;
	}
	else
	{
		cur_y = y_intermediate;
		cur_x = x_intermediate;
	}

	// offset
	cur_y += offsetY;
	cur_x += offsetX;

 	// new values
	*y = cur_y;
	*x = cur_x;

	return true;
}
//---------------------------------------------------------------------------
/*!
 * \brief Set the mode of processing data:
 *
 * \param[in] threadMode Requested mode: 0 - data measurement, 1 - video capturing
 */
void TDataStream::SetMode(THREADMODE threadMode)
{
	_threadMode=threadMode;
 	_commands.push("status "+IntToStr(udp->Binding->Port));
	_commands.push("single_req "+IntToStr((int)_threadMode));
}
//---------------------------------------------------------------------------
/*!
 * \brief Request for exprorting first fully captured frame to file
 */
void TDataStream::Export()
{
	log->Info("Request for exporting frame");
	_exportFrame = true;
}
//---------------------------------------------------------------------------
/*!
 * \brief Save first fully captured frame into file
 */
void TDataStream::_SaveFrame()
{

	String FileName=ExtractFilePath(Application->ExeName)+
		"\\Saved frames\\Frame_"+FormatDateTime("dd_MM_yyyy hh_mm_ss",Now())+".txt";

	log->Info("Saving frame as " +FileName);
	_sl->SaveToFile(FileName);
	ShowMessage("���� ��������: "+FileName);
	_exportFrame=false;
	_slAllowRec=false;
	_sl->Clear();
}
//---------------------------------------------------------------------------
/*
 * \brief Update all vizualization on chart and recalculate the shapes
 */
void __fastcall TDataStream::_UpdFrame()
{
	_callbackFrame(_xValues,_yValues,_xyValues,_fragmentType);
}
//---------------------------------------------------------------------------
/*
 * \brief Update video frame
 */
void __fastcall TDataStream::_UpdVideo()
{
	_callbackVideo(*_bmp);
}
//---------------------------------------------------------------------------
/*!
 * \brief Show the exception thrown by the thread
 */
void __fastcall TDataStream::_ShowException()
{
	_callbackExc(*_exception);
}
//---------------------------------------------------------------------------
/*!
 * \brief Check connection status of the thread
 */
bool TDataStream::IsConnected()
{
	return _connected;
}
//---------------------------------------------------------------------------
/*!
 * \brief Send the given command
 */
void TDataStream::Send(NetCmd cmd)
{
	_commands.push(cmd.sRequest);
}
//---------------------------------------------------------------------------
/*!
 * \brief Write data to register in a sensor
 */
void TDataStream::Write(String Command, String Addr, String Mask, String Value)
{
	_commands.push(Command + Addr + Mask + Value);
}
//---------------------------------------------------------------------------
/*!
 * \brief Write data to register in a sensor
 */
void TDataStream::Write(String Msg)
{
	_commands.push(Msg);
}
//---------------------------------------------------------------------------
/*!
 * \brief Write data to register in a sensor
 */
//void TDataStream::Write(int Addr, int Mask, int Value)
//{
//	_commands.push("confset" + IntToHex(Addr,4) + IntToHex(Mask,4) + IntToHex(Value,4));
//}
//---------------------------------------------------------------------------
/*!
 * \brief Write data to register in a sensor
 */
String TDataStream::Read(String Command)
{
	log->Info("Called Read"+Command);
	bool ReconnectRequired = IsConnected();
	String sResponse;

	Disconnect();
	while(IsConnected())
	{
		Application->ProcessMessages();
	}
	udp->ReceiveString(10,0);	//clear network buffer
	log->SetMinSize(1);
	log->Info("Network client disconnected, stream stopped. Sending \""+Command+"\"");
	udp->Send(Command);
	sResponse =	udp->ReceiveString(100,0);

	log->Info("Response: "+sResponse);

	log->SetMinSize(_minLogSize);
	if(ReconnectRequired)
	{
		Connect();
	}
	return sResponse;

}
//---------------------------------------------------------------------------
/*!
 * \brief Write data to register in a sensor
 */
//int TDataStream::Read(int Addr)
//{
//	return ReadReg(IntToHex(Addr,4)).ToInt();
//}
//---------------------------------------------------------------------------
/*!
 * \brief Set particular parameter
 */
void TDataStream::SetParameter(THREADPAR Par, double value)
{
	_parameters.at((int)Par) = value;
}


