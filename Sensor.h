#ifndef OMSSensorH
#define OMSSensorH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.DBChart.hpp>
#include <VCLTee.Series.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <VCLTee.TeeSpline.hpp>

#include "calculations.h"
#include "Calibration.h"
#include "data.h"
#include "Filter.h"
#include "graph.h"
#include "graph_observers.h"
#include "Measurements.h"
#include "NetCommands.h"
#include "point.h"
#include "regions.h"
#include "shapes.h"
#include "thrDataStream.h"
#include "thrLog.h"
#define DLLEXPORT __declspec (dllexport)
//---------------------------------------------------------------------------
typedef void CallbackFrame_t(vector<double>,vector<double>,vector<Point2D>,int);
typedef void CallbackVideo_t(TBitmap&);
typedef void CallbackException_t (Exception&);
class TableShape;
class TableRegion;
class Graph;
class RawData;
class DLLEXPORT TOMSSensor
{
	public:
	   TOMSSensor(){};

		void Init(TForm * Form, TDBChart * Chart,
			TStringGrid * sgRegions, TStringGrid * sgShapes, TStringGrid * sgCalculations,
		CallbackFrame_t CallbackFrame, CallbackVideo_t CallbackVideo, CallbackException_t CallbackExc);

		Graph * graph;
		TableRegion * regions;
		TableShape * shapes;
		TableCalculation * calculations;
		RawData * data;
		TCustomSeries * series;
		Log * log;
		TDataStream *thrDataStream;

};
#endif
