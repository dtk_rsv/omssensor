#ifndef CalibrationH
#define CalibrationH

class Calibration
{
public:
	bool Init(const std::string filename);

	double GetPoly9Coeff(unsigned int num)const;
//	double GetPoly2Coeff(unsigned int row,unsigned int col)const;
	double GetNormalizedMean()const { return _mean; }
	double GetNormalizedStd()const 	{ return _std; }

	double GetNormalizedMeanZ()const { return _meanZ; }
	double GetNormalizedStdZ()const 	{ return _stdZ; }

	double GetOffset()const { return _offset; }
	double GetReflection()const { return _reflection; }

	double Poly9(const double cur_val);
	double Matrix(const double cur_x, const double cur_y);

	Calibration();

private:
	enum { MinLevel=0, MaxLevel=10, MaxLevelMatrix=3 };

	// Polynom coefficients
	// f(x) = p1*x^9 + p2*x^8 + p3*x ^7+ p4*x^6 + p5*x^5 + p6*x^4 + p7*x^3 + p8*x^2 + p9*x + p10
	double _poly9[MaxLevel];
	// Matrix coefficients
	// f(x,y) = p00+p10x+p01y
	double _polyMatrix[3][3];
	// Normalize coefficients
	double _mean;
	double _std;
	double _meanZ;
	double _stdZ;
	double _offset;		// offset
	double _reflection;  // Reflection (-1 - reflect signal, 1 - no reflection)
};
#endif
