#pragma hdrs+top
#include "Filter.h"

Filter::Filter(size_t wnd_sz) : wnd_size(wnd_sz) {}

void Filter::Set(double value)
{
	if(data.size() >= wnd_size) {
		data.pop_front();
	}
	data.push_back(value);
}

double Filter::Get() const
{
	const size_t hwsz = wnd_size / 2;
	// algorithm don't work if count of poins less then window
	if(data.size() < wnd_size) return 0;

	std::vector<double> wnd(wnd_size);
	std::copy(data.begin(), data.end(), wnd.begin());
	std::sort(wnd.begin(), wnd.end());
	return wnd[hwsz];
}

String Filter::GetStr() const
{
	String str;
	for(size_t n = 0; n < data.size(); ++n) {
		str += FloatToStrF( data[n],ffGeneral,7,3);
		str += " ";
	}
	return str;
}
#pragma package(smart_init)
