#ifndef graph_observersH
#define graph_observersH

#include <Grids.hpp>
#include <map>
#include <vector>
#include <list>
#include "graph.h"
using namespace std;

//------------------------------------------------------------------------------
/*!
 * Abstract graphical observer
 */
class GraphObserver
{
public:
	GraphObserver(Graph * g, TForm * Owner);
	virtual ~GraphObserver(){};
	virtual void update(GRAPH_EVENT e, Region * r) = 0;
	virtual void update(GRAPH_EVENT e, Shape * s) = 0;
	virtual void update(GRAPH_EVENT e, Calculation * c) = 0;

private:

protected:
	map<int,bool> ItemsSelected;
	bool IsSelectStatus;
	TForm * owner;
	Graph * graph;
	TStringGrid * _grid;
};
//------------------------------------------------------------------------------
/*!
 * \brief Observer of the table of regions
 */
class TableRegion : public GraphObserver
{

	public:
		TableRegion(Graph * graph, TForm * owner, TStringGrid* grid);
		virtual ~TableRegion() {}
		void update(GRAPH_EVENT e, Region * r);
		void update(GRAPH_EVENT e, Shape * s) {};
		void update(GRAPH_EVENT e, Calculation * c) {};

		Region * GetCurrentRegion() const;
		void SetSelectStatus(bool status);
		void GetSelected(std::vector<Region *> & selected_regions);

		void ShowClicked(bool TrueFalse);
		void Show();
		void Hide();

	private:
		bool _visible;
		bool _showClicked;
		size_t count_row;
		size_t curr_row;
		std::list<Region *> regions;
		std::list<Region *>::iterator _pos;

		void __fastcall GridMouseDown(TObject *Sender, TMouseButton Button,
			TShiftState Shift, int X, int Y);
		void __fastcall GridClick(TObject *Sender);
		void __fastcall GridKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);
		void __fastcall GridMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
			  int X, int Y);
		void Configure(const std::string fn);
};
//------------------------------------------------------------------------------
/*!
 * \brief Observer of the table of shapes
 */
class TableShape : public GraphObserver
{

	public:
		TableShape(Graph * graph, TForm * owner, TStringGrid* grid);
		virtual ~TableShape() {}
		void update(GRAPH_EVENT e, Region * r) {};
		void update(GRAPH_EVENT e, Shape * s);
		void update(GRAPH_EVENT e, Calculation * c) {};

		Shape * GetCurrentShape() const;
		void SetSelectStatus(bool status);
		void GetSelected(std::vector<Shape *> & selected_shapes);
		void GetAll(std::list<Shape*> * shapes);

		void ShowClicked(bool TrueFalse);
		void Show();
		void Hide();

		void ResetAll();
		void first();
		void next();
		void last();

	private:
		bool _visible;			//visible flag for all shapes
		bool _showClicked;      //show only clicked shape or not
		size_t _count_row;
		size_t _curr_row;
		std::list<Shape *> _shapes;
		std::list<Shape *>::iterator _pos;

		void __fastcall GridClick(TObject *Sender);
		void __fastcall GridKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);

		void __fastcall GridDrawCell(TObject *Sender, int ACol, int ARow,  const TRect &Rect,
			  TGridDrawState State);
		void __fastcall GridMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
			  int X, int Y);

		void Configure(const std::string fn);
		void DeleteShape();
		void DeleteShape(Shape * s);
};
//------------------------------------------------------------------------------
/*!
 * \brief Observer of the table of calculations
 */
class TableCalculation : public GraphObserver
{

	public:
		TableCalculation(Graph * graph, TForm * owner, TStringGrid* grid);
		virtual ~TableCalculation() {}
		void update(GRAPH_EVENT e, Region * r) {};
		void update(GRAPH_EVENT e, Shape * s) {};
		void update(GRAPH_EVENT e, Calculation * c);
		Calculation * GetCurrentCalculation() const;
		void SetSelectStatus(bool status);
		void GetSelected(std::vector<Calculation *> & selected_calculations);
		void GetAll(std::list<Calculation*> * calculations);
		void ShowAux();
		void HideAux();
		void ShowMarks();
		void HideMarks();
		void ResetAll();
		void first();
		void next();
		void last();

	private:

		size_t _count_row;
		size_t _curr_row;
		std::list<Calculation *> _calculations;
		std::list<Calculation *>::iterator _pos;

		void __fastcall GridClick(TObject *Sender);
		void __fastcall GridKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);

		void __fastcall GridDrawCell(TObject *Sender, int ACol, int ARow,  const TRect &Rect,
			  TGridDrawState State);
		void __fastcall GridMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
			  int X, int Y);

		void Configure(const std::string fn);
		void DeleteCalculation();
		bool _auxVisible;
		bool _marksVisible;
};
#endif
