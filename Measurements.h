//---------------------------------------------------------------------------

#ifndef MeasurementsH
#define MeasurementsH
#include "Sensor.h"
//---------------------------------------------------------------------------
typedef struct
{
	int RegionType;
	String RegionStr;
	int ShapeType;
} sMeasureParams;

class TMeasurement
{
	public:
		TMeasurement(RawData * data, Graph * graph);

		double MakeMeasurement(int MeasureType, std::vector <sMeasureParams> params, String * ShapeStr);
		bool CreateRegion (Region::REGIONTYPE RegionType, vector<Region*> Regions);
		void CreateSpecifiedRegion(Region::REGIONTYPE RegionType, String RegionStr);
		bool CreateShape(Shape::SHAPETYPE ShapeType, Region * R);

		double Calculate(Calculation::CALCTYPE CalcType, std::vector <Shape*> shapes);
		void CalculateAll(TableCalculation * c, TableShape * s);
		void CalculateShapes(TableShape * s);
	private:
		RawData * _data;
		Graph * _graph;
};
#endif
