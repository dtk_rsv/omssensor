//---------------------------------------------------------------------------
#ifndef thrLogH
#define thrLogH
#include <vcl.h>
#include <System.hpp>
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <sstream>
#include <fstream>
#include <queue>
#include <iomanip>
using namespace std;

enum STATUS {ST_ERROR, ST_WARNING, ST_INFO};
 #define DLLEXPORT __declspec (dllexport)
class DLLEXPORT Log// : public TThread
{
	public:
		Log(const String filename, unsigned int buffer_size);

		Log(STATUS Status);
		Log(STATUS Status, const String ClassName, const String MethodName, const String Message);
//
		void SetClassName(String ClassName);
		void SetMethodName(String MethodName);
		void SetMessage(String Message);
		void SetStatus(STATUS Status);
		void Write(STATUS Status, String msg);

		void Info(const String);  		//use native types here
		void Warning(const String);
		void Error(const String);

		void SetMinSize(unsigned int size);

		void Close();

	protected:
		void __fastcall Execute();

	private:
		void __fastcall Release();
		bool _active;
		unsigned int _buffer_size;
		unsigned int _min_buffer_size;
		String _filename;
		String classname;
		String methodname;
		TStringList * _buffer;
		STATUS status;
		friend DWORD WINAPI thLog(void*);
		HANDLE hLog;

		bool Terminated;
};

//---------------------------------------------------------------------------
#endif

