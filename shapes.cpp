#pragma hdrstop
#include "shapes.h"
#include <math.h> //degtograd
//alglib
#include <ap.h>
#include <interpolation.h>
#include <LinAlg.h>
#include <alglibinternal.h>
#include <statistics.h>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
using namespace alglib;
//------------------------------------------------------------------------------
// Shape
//------------------------------------------------------------------------------
Shape::Shape(Region * r, Graph * g, const std::string & n, int t, const TColor color)
	: region(r), graph(g), name(n), type(t), _color(color)
{
	graph->AddShape(this);
	r->myShapes.push_back(this);
}
//------------------------------------------------------------------------------
Region * Shape::GetRegionAndShapeType(int * type) const
{
	*type=this->type;
	return region;
}
//------------------------------------------------------------------------------
Shape::~Shape()
{
	//delete the visualization part of the shape
	graph->DeleteShape(this);

	int sz = myCalcs.size();
	//delete all calculations connected with this shape
	for(int i = 0; i < sz; i++)
	{
		Calculation * c = myCalcs.at(0);  	//don't increment here because container's elements
		graph->notify(DELETED,c);           //always move to zero position
	}

	//delete this shape from region's vector
	region->myShapes.erase(find(region->myShapes.begin(),region->myShapes.end(),this));
}
//------------------------------------------------------------------------------
void Shape::Show()
{
	_series->Visible = true;
}
//------------------------------------------------------------------------------
void Shape::Hide()
{
    _series->Visible = false;
}
//------------------------------------------------------------------------------
void Shape::MakeSelection(bool ShowNeeded)
{
	_series->LinePen->Width=3;
	if(ShowNeeded)
	{
		_series->Visible = true;
	}
}
//------------------------------------------------------------------------------
void Shape::RemoveSelection(bool HideNeeded)
{
	_series->LinePen->Width=1;
	if(HideNeeded)
	{
		_series->Visible = false;
	}
}
//------------------------------------------------------------------------------
// Circle
//------------------------------------------------------------------------------
TCircle::TCircle(Region * r, Graph * g) : Shape(r, g, "circle",CIRCLE,clMaroon), _x0(0), _y0(0), _R(0)
{
	_series = new TLineSeries(g->GetContext());
	_series->ParentChart = g->GetContext();
	_series->SeriesColor = _color;
	_series->XValues->Order = loNone;
	_series->YValues->Order = loNone;

	_series->Legend->Visible = false;

	graph->NewShapeCreated(this);

	++Count();
	_index=Count();
}
//------------------------------------------------------------------------------
TCircle::~TCircle()
{
	--Count();
	_index=Count();
	delete _series;
}
//------------------------------------------------------------------------------
//approximation of points with circle: http://www.prografix.narod.ru/rus_cirap.html
bool TCircle::Calculate()
{
	std::vector<double> x, y;
	region->GetPointsInside(x, y);

	double N11, N12, N21, N22, W1, W2, det;
	double sx=0, sx2=0, sy=0, sy2=0, sxy=0, sx3=0, sy3=0, sxy2=0, sx2y=0;	//summs of coordinates
	int sz = x.size();

	if(sz == 0)	return false;
	double n = static_cast<double> (sz);

	std::vector<double>::iterator ix, iy;
	std::vector<double> x2, y2; 	//second power of x,y
	std::vector<double> x3, y3;     //third power of x,y

	ix = x.begin();
	iy = y.begin();

	for(;ix!=x.end(); ++ix, ++iy)
	{
		double valx, valy;
		valx = (*ix) * (*ix);
		valy = (*iy) * (*iy);
		x2.push_back(valx);
		y2.push_back(valy);

		valx = (*ix) * (*ix) * (*ix) ;
		valy = (*iy) * (*iy) * (*iy) ;
		x3.push_back(valx);
		y3.push_back(valy);
	}

	sx=accumulate(x.begin(), x.end(), sx);         	//calculate summs of first power
	sy=accumulate(y.begin(), y.end(), sy);          //of x and y

	sx2=accumulate(x2.begin(), x2.end(), sx2);     //calculate summs of second power
	sy2=accumulate(y2.begin(), y2.end(), sy2);     //of x and y

	sx3=accumulate(x3.begin(), x3.end(), sx3);     //calculate summs of thirs power
	sy3=accumulate(y3.begin(), y3.end(), sy3);     //of x and y

	sxy=inner_product(x.begin(), x.end(), y.begin(), sxy);       //calculate summ of binary multiples
	sxy2=inner_product(x.begin(), x.end(), y2.begin(), sxy2);    //like x0*y0+x1*y1+x2*y2
	sx2y=inner_product(x2.begin(), x2.end(), y.begin(), sx2y);   //for [x]*[y], [x]*[y2] and [x2]*[y]

	N11 = 2*(sx2 - (1/n)*sx*sx);
	N12 = 2*(sxy - (1/n)*sx*sy);
	W1 = (sx3 + sxy2 - (1/n)*sx2*sx - (1/n)*sy2*sx);

	N21 = 2*(sxy - (1/n)*sx*sy);
	N22 = 2*(sy2 - (1/n)*sy*sy);
	W2 = (sx2y + sy3 - (1/n)*sx2*sy - (1/n)*sy2*sy);

	try
	{
		det = N11*N22 - N12*N21;

		_x0 = (W1*N22 - W2*N12) / det;
		_y0 = (W2*N11 - W1*N21) / det;
		_R = (_x0*_x0) + (_y0*_y0) + (1/n)*(sx2 + sy2 - 2*(_x0*sx + _y0*sy));
		_R = sqrt(_R);

	}
	catch(...)
	{
		//todo:log errors
		return false;	//e.g. division by zero
    }
	graph->notify(GRAPH_EVENT::UPDATE, this);
	return true;
}
//------------------------------------------------------------------------------
std::string TCircle::GetResults(std::vector<double> & results)
{
	results.push_back(_x0);
	results.push_back(_y0);
	results.push_back(_R);
	return name;
}
//------------------------------------------------------------------------------
void TCircle::Assign()
{
	for(double i=0; i<=360; i++)
	{
		double rad = (i * 3.1415) / 180;
		double yy = _y0 + cos(rad) * _R;
		double xx = _x0 + sin(rad) * _R;
		_series->AddXY(xx, yy);
	}
}
//------------------------------------------------------------------------------
void TCircle::Update()
{
	_series->Clear();
	Assign();
}
//------------------------------------------------------------------------------
void TCircle::Clear()
{
	_series->Clear();
}
//------------------------------------------------------------------------------
std::string TCircle::GetFullName() const
{
	return name+" "+boost::lexical_cast<std::string>(_index);
}
//------------------------------------------------------------------------------
// Line
//------------------------------------------------------------------------------
TLine::TLine(Region * r, Graph * g)
	: Shape(r, g, "line",LINE,clLime), _k(0), _b(0)
{
	const size_t point_size = 2;
	_series = new TLineSeries(g->GetContext());
	_series->ParentChart = g->GetContext();
	_series->SeriesColor = _color;
	_series->XValues->Order = loNone;
	_series->YValues->Order = loNone;

	_series->Legend->Visible = false;

	_series->Pointer->Visible = true;
	_series->Pointer->Style = psCircle;
	_series->Pointer->VertSize = point_size;
	_series->Pointer->HorizSize = point_size;

	graph->NewShapeCreated(this);

	++Count();	//increment the count of lines
	_index=Count();
}
//------------------------------------------------------------------------------
TLine::~TLine()
{
	--Count();
	_index=Count();
	delete _series;
}
//------------------------------------------------------------------------------
bool TLine::Calculate()
{
	std::vector<double> x, y;
	region->GetPointsInside(x, y);
	double sx = 0, sx2 = 0, sy = 0, sxy = 0, angle, atn;
	int sz = x.size();
	if(sz == 0)
	{
		return false;
	}

	sx = std::accumulate(x.begin(), x.end(), sx);				//calculate sum of x values
	sy = accumulate(y.begin(), y.end(), sy);        			//calculate sum of y values
	sx2 = inner_product(x.begin(), x.end(), x.begin(), sx2); 	//calculate sum of
	sxy = inner_product(x.begin(), x.end(), y.begin(), sxy);    //binary multiplies for x*x,x*y
	int n = x.size();
	double D = n * sx2 - sx * sx;
	if (D != 0)
	{
		try
		{
			_b = (sy * sx2 - sx * sxy) / D;    						//b coefficient
			_k = (n * sxy - sx * sy) / D;                           //k coefficient
			if(fabs(_k)==1)
			{
				_angleX = _k*45;
			}
			else
			{
				angle = (_k) / (1 + _k);
				atn = atan(angle);
				_angleX = atn * 180.0 / M_PI;                           //tilt angle from bottom axes
			}
			_angleY = 90 - _angleX;                                 //tilt angle from left axes

			region->GetDataBoundariesX(_x1, _x2);                   //x1 and x2 of line segment
			_y1 = _k * _x1 + _b;                                    //y1 and y2 of line segment
			_y2 = _k * _x2 + _b;

			_L=sqrt(pow((_x2-_x1),2)+pow((_y2-_y1),2));              //length of line setment

			graph->notify(GRAPH_EVENT::UPDATE, this);	//todo:move this call outside for synchronization with form
			return true;
		}
		catch (...)
		{
			return false;
        }
	}
	else return false;
}
//------------------------------------------------------------------------------
std::string TLine::GetResults(std::vector<double> & results)
{
	results.push_back(_k);
	results.push_back(_b);
	results.push_back(_L);
	results.push_back(_angleX);
	results.push_back(_angleY);
	results.push_back(_x1);
	results.push_back(_y1);
	results.push_back(_x2);
	results.push_back(_y2);
	return name;
}
//------------------------------------------------------------------------------
void TLine::Assign()
{
	_series->AddXY(_x1, _y1);
	_series->AddXY(_x2, _y2);
}
//------------------------------------------------------------------------------
void TLine::Update()
{
	_series->Clear();
	Assign();
}
//------------------------------------------------------------------------------
void TLine::Clear()
{
	_series->Clear();
}
//------------------------------------------------------------------------------
std::string TLine::GetFullName() const
{
	return name+" "+boost::lexical_cast<std::string>(_index);
}
//------------------------------------------------------------------------------
// Ellipse
//------------------------------------------------------------------------------
TEllipse::TEllipse(Region * r, Graph * g) : Shape(r, g, "ellipse",ELLIPSE,clFuchsia),
			_X0(0),_Y0(0),_DX0(0),_DY0(0),_Alpha(0),_LX(0),_LY(0)
{
	_series = new TLineSeries(g->GetContext());
	_series->ParentChart = g->GetContext();
	_series->SeriesColor = _color;
	_series->XValues->Order = loNone;
	_series->YValues->Order = loNone;

	_series->Legend->Visible = false;

	++Count();
    _index=Count();
	graph->NewShapeCreated(this);
}
//------------------------------------------------------------------------------
TEllipse::~TEllipse()
{
	--Count();
	_index=Count();
	delete _series;
}
//------------------------------------------------------------------------------
/*!
 * \brief Ellipsis approximation of points.
 * \author Yuri Bochkarev
 */
bool TEllipse::Calculate()
{
	//todo:division by zero trycatch
	std::vector<double> x, y;
	region->GetPointsInside(x, y);	//get points inside of region

	alglib::real_2d_array res;
	alglib::real_2d_array D1, D2; // :array[0..5,0..2] of double;
	alglib::real_2d_array D1T, D2T; // :array [0..2,0..5] of double;
	alglib::real_2d_array S1, S2, S3, S2T, T, M, M1;
	// : array [0..2,0..2] of double;
	int i, j, k, N; // :integer;
	// matinvreport rep;
	alglib::real_2d_array S3i, Me;
	alglib::real_1d_array WR, WI;
	alglib::real_2d_array VL, VR;
	float a, b, c, d, e, f;
	float ba, bb, bc, bd, bf, bg;
	float CX, CY, DX, DY, Numer, Den1, Den2, Den3;
	alglib::matinvreport rep;

	res.setlength(2,x.size());	//length of data array

	for(unsigned int vCount=0;vCount<x.size();vCount++)   	//set x values to alglib array
	{
		res[0][vCount]=x.at(vCount);
	}

	for(unsigned int vCount=0;vCount<x.size();vCount++)    	//set y values to alglib array
	{
		res[1][vCount]=y.at(vCount);
	}
	N = res.cols();
	D1.setlength(N, 3);
	D2.setlength(N, 3);
	D1T.setlength(3, N);
	D2T.setlength(3, N);
	S1.setlength(3, 3);
	S2.setlength(3, 3);
	S3.setlength(3, 3);
	S2T.setlength(3, 3);
	S3i.setlength(3, 3);
	T.setlength(3, 3);
	M.setlength(3, 3);
	M1.setlength(3, 3);
	Me.setlength(3, 3);
	WR.setlength(3);
	WI.setlength(3);
	VL.setlength(3, 3);
	VR.setlength(3, 3);

	for (i = 0; i < N; i++)
	{
		D1[i][0] = res[0][i] * res[0][i]; // X[i]*X[i];
		D2[i][0] = res[0][i];
		D1[i][1] = res[0][i] * res[1][i]; // X[i]*Y[i];
		D2[i][1] = res[1][i]; // Y[i];
		D1[i][2] = res[1][i] * res[1][i]; // Y[i]*Y[i];
		D2[i][2] = 1;

	}
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < 3; j++)
		{
			D1T[j][i] = D1[i][j];
			D2T[j][i] = D2[i][j];
		}
	}

	for (j = 0; j < 3; j++)
	{
		for (k = 0; k < 3; k++)
		{
			S1[k][j] = 0.0;
			S2[k][j] = 0.0;
			S3[k][j] = 0.0;
			for (i = 0; i < N; i++)
			{
				S1[k][j] = S1[k][j] + D1T[k][i] * D1[i][j];
				S2[k][j] = S2[k][j] + D1T[k][i] * D2[i][j];
				S3[k][j] = S3[k][j] + D2T[k][i] * D2[i][j];

			}
			S2T[j][k] = S2[k][j];
		}
	}

	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			S3i[i][j] = S3[i][j];
		}
	}
	rmatrixinverse(S3i, 3, j, rep);

	for (j = 0; j < 3; j++)
	{
		for (k = 0; k < 3; k++)
		{
			T[k][j] = 0;
			for (i = 0; i < 3; i++)
			{
				T[k][j] += (-1) * S3i[k][i] * S2T[i][j];
			}
		}
	}

	for (j = 0; j < 3; j++)
	{
		for (k = 0; k < 3; k++)
		{
			M[k][j] = S1[k][j];
			for (i = 0; i < 3; i++)
			{
				M[k][j] += S2[k][i] * T[i][j];
			}
		}
	}
	for (j = 0; j < 3; j++)
	{
		Me[0][j] = (M[2][j]) / 2;
		Me[1][j] = -M[1][j];
		Me[2][j] = (M[0][j]) / 2;

	}

	rmatrixevd(Me, 3, 3, WR, WI, VL, VR);

	for (i = 0; i < 3; i++) 	//Calculation of polynomial coefficients
	{
		if (4 * VR[0][i] * VR[2][i] - VR[1][i] * VR[1][i] > 0)
		{
			a = VR[0][i];
			b = VR[1][i];
			c = VR[2][i];
			d = T[0][0] * VR[0][i] + T[0][1] * VR[1][i] + T[0][2] * VR[2][i];
			e = T[1][0] * VR[0][i] + T[1][1] * VR[1][i] + T[1][2] * VR[2][i];
			f = T[2][0] * VR[0][i] + T[2][1] * VR[1][i] + T[2][2] * VR[2][i];
			break;
		}
	}
	ba = a;
	bb = b / 2;
	bc = c;
	bd = d / 2;
	bf = e / 2;
	bg = f;
	try
	{
		CX = (bc * bd - bb * bf) / (bb * bb - ba * bc);
		CY = (ba * bf - bb * bd) / (bb * bb - ba * bc);
		Numer = 2 * (ba * bf * bf + bc * bd * bd + bg * bb * bb -
			2 * bb * bd * bf - ba * bc * bg);
		Den1 = bb * bb - ba * bc;
		Den2 = sqrt((ba - bc) * (ba - bc) + 4 * bb * bb) - (ba + bc);
		Den3 = -sqrt((ba - bc) * (ba - bc) + 4 * bb * bb) - (ba + bc);

		DX = 2 * sqrt(Numer / (Den1 * Den2));
		DY = 2 * sqrt(Numer / (Den1 * Den3));

		_LY=fabs(sqrt((2*bb*CX+2*bf)*(2*bb*CX+2*bf)-4*bc*(ba*CX*CX+2*bd*CX+bg))/bc);
		_LX=fabs(sqrt((2*bb*CY+2*bd)*(2*bb*CY+2*bd)-4*ba*(bc*CY*CY+2*bf*CY+bg))/ba);

		if (bb==0)
		{
			if (ba<=bc)
			{
				_Alpha=0;
			}
			else
			{
				_Alpha=90;
			}

		}
		else
		{
			_Alpha=180*((M_PI/2)+0.5*ArcCot((ba-bc)/(2*bb)))/M_PI;
		}

		if (DX>DY)
		{
			_DX0 = DX; // *0.995025+1.484577;
			_DY0 = DY; // *1.0004+0.643257;
		}
		else
		{
			_DY0 = DX; // *0.995025+1.484577;
			_DX0 = DY; // *1.0004+0.643257;
		}

		_X0 = CX;
		_Y0 = CY;

		graph->notify(GRAPH_EVENT::UPDATE, this);   	//rsv
		return true;
	}

	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		_DX0 = 0;
		_DY0 = 0;
		_X0 = 0;
		_Y0 = 0;
		_Alpha=0;
		return false;
	}
}
//------------------------------------------------------------------------------
std::string TEllipse::GetResults(std::vector<double> & results)
{
	results.push_back(_X0);         //the center of the ellipse at X
	results.push_back(_Y0);         //the center of the ellipse at Y
	results.push_back(_DX0);        //small semi-axes
	results.push_back(_DY0);        //big semi-axes
	results.push_back(_Alpha); 		//angle of ellipse
	results.push_back(_LX);         //bottom axes proection
	results.push_back(_LY);         //left axes proection
	return name;
}
//------------------------------------------------------------------------------
/*!
 * \brief Draw rotated ellipse
 *
 * The method of rotation is taken from http://delphimaster.net/view/8-67921
 */
void TEllipse::Assign()
{
	int i,n;  					//index and count of iterations
	double ang;    				//desired angle of rotation of the ellipse
	double sfi,cfi,alpha;  		//sin(fi),cos(fi), alpha(don't know what is it)
	double xa,ya;               //x and y in canonical coordinate system
	double x,y;                 //x and y in rotated coordinate system
	double a,b;                 //big and small semi-axes of the ellipse
	double x0,y0;               //coordinates of the center of the ellipse

	ang=_Alpha-90;
	x0=_X0;
	y0=_Y0;
	a=_DX0/2;
	b=_DY0/2;
	n=360;
	sfi=sin(DegToRad(ang));
	cfi=cos(DegToRad(ang));
	for (i=0;i<=n;i++)
	{
		alpha=i*2*M_PI/n;
		xa=a*cos(alpha);
		ya=b*sin(alpha);
		x=x0-xa*cfi+ya*sfi;
		y=y0-xa*sfi+ya*cfi;
		_series->AddXY(x,y);
	}
}
//------------------------------------------------------------------------------
void TEllipse::Update()
{
	_series->Clear();
	Assign();
}
//------------------------------------------------------------------------------
void TEllipse::Clear()
{
	_series->Clear();
}
//------------------------------------------------------------------------------
std::string TEllipse::GetFullName() const
{
	return name+" "+boost::lexical_cast<std::string>(_index);
}
#pragma package(smart_init)
