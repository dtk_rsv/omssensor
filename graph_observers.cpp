﻿#pragma hdrstop
#include "graph_observers.h"
#include "graph.h"
#include "shapes.h"
#include "regions.h"
#include "calculations.h"
class Graph;
//------------------------------------------------------------------------------
// Abstract observer

GraphObserver::GraphObserver(Graph * g, TForm* Owner)
	: graph(g), owner(Owner)
{
	graph->attach(this);
}

//---------------------------------------------------------------------------
// TableRegion
TableRegion::TableRegion(Graph * graph, TForm* Owner, TStringGrid * grid)
		: GraphObserver(graph, Owner), count_row(0)//, pos(NULL)
{
	_showClicked = false;
	_visible = true;
	_grid=grid;
	// Grid events
	_grid->OnClick = GridClick;
	_grid->OnKeyUp = GridKeyUp;
	_grid->OnMouseUp = GridMouseUp;

}
//------------------------------------------------------------------------------
void __fastcall TableRegion::GridMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
	int X, int Y)
{
	if(IsSelectStatus)
	{
		int col, row;
		_grid->MouseToCell(X, Y, col, row);
		if ((row > 0) && (row <= _grid->RowCount - 1))
		{
			if (ItemsSelected[_grid->Row - 1])
				ItemsSelected[_grid->Row - 1] = false;
			else
				ItemsSelected[_grid->Row - 1] = true;
		}
		_grid->Invalidate();
	}
}
//------------------------------------------------------------------------------
void __fastcall TableRegion::GridClick(TObject *Sender)
{
	if(_pos == NULL) return;

	(*_pos)->RemoveSelection(_showClicked);

	curr_row = _grid->Row;
	size_t row = _grid->FixedRows;
	_pos = regions.begin();
	while (row < curr_row && _pos != regions.end())
	{
		row++;
		_pos++;
	}
	if(_pos == regions.end()) return;

	(*_pos)->MakeSelection(_showClicked);
}

//------------------------------------------------------------------------------
 void __fastcall TableRegion::GridKeyUp(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if(Key == VK_DELETE)
    {

		if(_pos == NULL) return;
		// delete current region
		delete (*_pos);
		// change pointer to current region
		if(_pos == regions.begin())
        {
			if(regions.size() == 1)
            {
				regions.erase(_pos);
				_pos = NULL;
			}
			else
            {
				regions.erase(_pos++);
			}
		}
		else
        {
			regions.erase(_pos--);
		}

		for(unsigned int n = curr_row; n < count_row; n++)
        {
			_grid->Cells[0][n] = UIntToStr(n);
			_grid->Cells[1][n] = _grid->Cells[1][n+1];
			_grid->Cells[2][n] = _grid->Cells[2][n+1];
		}

		if(count_row > 1)
        {
			_grid->RowCount = count_row;
			if(curr_row == count_row)
				curr_row--;
			count_row--;
			_grid->Row = curr_row;

		}
		else
        {
			_grid->Cells[0][count_row] = "";
			_grid->Cells[1][count_row] = "";
			_grid->Cells[2][count_row] = "";
			count_row--;
		}
		_grid->OnClick(_grid);
	}
}

//------------------------------------------------------------------------------
void TableRegion::update(GRAPH_EVENT e, Region * r)
{
	switch(e)
    {
		case CREATED:
			if(_pos != NULL)
            {
				(*_pos)->RemoveSelection(_showClicked);
			}
			regions.push_back(r);
			_pos = regions.end();
			_pos--;

			_grid->RowCount = ++count_row + 1;
			curr_row = count_row;

			double left, right;
			r->GetRegionBoundariesX(left, right);

			_grid->Cells[0][curr_row] = count_row;
			_grid->Cells[1][curr_row] = r->GetName().c_str();
			_grid->Cells[2][curr_row] = "X1="+FormatFloat("00.0000",left)+" X2="+FormatFloat("00.0000",right);
			//todo:add all parameters
			_grid->Row = curr_row;

			//rsv:if we should ask for next region
//			GUI->SetState(TGuiThread::REQUEST_NEXT);
            break;

		case DELETED:

		break;
	}
}

//------------------------------------------------------------------------------
Region * TableRegion::GetCurrentRegion() const
{
	if(_pos != NULL)
		return *_pos;
	else return NULL;
}
//------------------------------------------------------------------------------
void TableRegion::SetSelectStatus(bool status)
{
	IsSelectStatus = status;
	if(IsSelectStatus)
	{
		for(int n=0; n < ItemsSelected.size(); n++)
		{
			ItemsSelected[n] = false;
		}
	}
	_grid->Repaint();
}
//------------------------------------------------------------------------------
void TableRegion::GetSelected(std::vector<Region *> & selected_regions)
{
	std::list<Region *>::iterator begin = regions.begin();
	std::list<Region* >::iterator end = regions.end();
	std::list<Region* >::iterator item;
	int num = 0;
	for(item = begin; item != end; ++item)
	{
		if(ItemsSelected[num])
		{
			selected_regions.push_back(*item);
		}
		num++;
	}
}
//------------------------------------------------------------------------------
void TableRegion::Show()
{
	std::list<Region *>::iterator it;
	for(it = regions.begin(); it != regions.end(); it++)
	{
		(*it)->Show();
	}
	_visible = true;
}
//------------------------------------------------------------------------------
void TableRegion::Hide()
{
	std::list<Region *>::iterator it;
	for(it = regions.begin(); it != regions.end(); it++)
	{
		(*it)->Hide();
	}
	_visible = false;
}
//------------------------------------------------------------------------------
void TableRegion::ShowClicked(bool TrueFalse)
{
	_showClicked = TrueFalse;
	if(TrueFalse == true)            //if this option is activated
	{
		std::list<Region *>::iterator it;
		for(it = regions.begin(); it != regions.end(); it++)
		{
			(*it)->Hide();
		}							//first hide all the shapes
		(*_pos)->Show();    		//then show the current one
	}
	else
	{
		_visible?Show():Hide();    //if deactivated - switch to the normal mode
	}
}
//------------------------------------------------------------------------------
// TableShape
//------------------------------------------------------------------------------
TableShape::TableShape(Graph * graph, TForm* Owner, TStringGrid * grid)
		: GraphObserver(graph, Owner), _count_row(0)
{
	_showClicked = false;
	_visible = true;
	_grid=grid;
	// Grid events
	_grid->OnDrawCell = GridDrawCell;
	_grid->OnClick = GridClick;
	_grid->OnKeyUp = GridKeyUp;
	_grid->OnMouseUp = GridMouseUp;
}
//------------------------------------------------------------------------------
void __fastcall TableShape::GridClick(TObject *Sender)
{
	if(_pos == NULL) return;

	(*_pos)->RemoveSelection(_showClicked);

	_curr_row = _grid->Row;
	size_t row = _grid->FixedRows;
	_pos = _shapes.begin();
	while (row < _curr_row && _pos != _shapes.end())
    {
		row++;
		_pos++;
	}
	if(_pos == _shapes.end()) return;

	(*_pos)->MakeSelection(_showClicked);
}

//------------------------------------------------------------------------------
 void __fastcall TableShape::GridKeyUp(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if(Key == VK_DELETE)
	{
		DeleteShape();
	}
}

//------------------------------------------------------------------------------
void __fastcall TableShape::GridDrawCell(TObject *Sender, int ACol, int ARow, const TRect &Rect,
	TGridDrawState State)
{
	if ((ACol == 0) || (ARow == 0))
		return;

}
//------------------------------------------------------------------------------

void __fastcall TableShape::GridMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
	int X, int Y)
{
	if(IsSelectStatus)
    {
		int col, row;
		_grid->MouseToCell(X, Y, col, row);
		if ((row > 0) && (row <= _grid->RowCount - 1))
        {
			if (ItemsSelected[_grid->Row - 1])
				ItemsSelected[_grid->Row - 1] = false;
			else
				ItemsSelected[_grid->Row - 1] = true;
		}
		_grid->Invalidate();
	}
}
//------------------------------------------------------------------------------
void TableShape::update(GRAPH_EVENT e, Shape * s)
{
	switch(e)
    {
		case CREATED:
		{
			if(_pos != NULL)
            {
				(*_pos)->RemoveSelection(_showClicked);
			}
			_shapes.push_back(s);
			_pos = _shapes.end();
			_pos--;

			_grid->RowCount = ++_count_row + 1;
			_curr_row = _count_row;

			std::vector<double> results;
			std::string str = s->GetResults(results);
			String who=str.c_str();
			_grid->Cells[0][_curr_row] = _curr_row;
			_grid->Cells[1][_curr_row] = who+" "+s->GetIndex();

			//todo:change names to IDs
			if(who == "line")
            {
				_grid->Cells[2][_curr_row] = "K="+FormatFloat("00.000",results[0])+" B="+FormatFloat("00.000",results[1])+" L="+FormatFloat("00.000",results[2])+
					" angleX="+FormatFloat("00.000",results[3]) + " angleY="+FormatFloat("00.000",results[4]);
			}
			if(who == "circle")
			{
				_grid->Cells[2][_curr_row] = "X0="+FormatFloat("00.000",results[0])+" Y0="+FormatFloat("00.000",results[1])+" R="+FormatFloat("00.000",results[2]);
			}
			if(who == "ellipse")
			{
				_grid->Cells[2][_curr_row] = "X0="+FormatFloat("00.000",results[0])+" Y0="+FormatFloat("00.000",results[1]) +
				  	" DX0="+FormatFloat("00.000",results[2])+" DY0="+FormatFloat("00.000",results[3])+" alpha="+FormatFloat("00.000",results[4]);
			}
			_grid->Row = _curr_row;
			break;
		}

		case UPDATE:
		{
			std::vector<double> results;
			std::string str = s->GetResults(results);
			String who=str.c_str();

			_grid->Cells[0][_curr_row] = _curr_row;
			_grid->Cells[1][_curr_row] = who+" "+s->GetIndex();;
			_grid->RowHeights[_curr_row]=_grid->RowHeights[0]*2;
			if(who == "line")
            {
				_grid->Cells[2][_curr_row] = "K="+FormatFloat("00.000",results[0])+" B="+FormatFloat("00.000",results[1])+" L="+FormatFloat("00.000",results[2])+
					" angleX="+FormatFloat("00.000",results[3]) + " angleY="+FormatFloat("00.000",results[4]);
			}
			if(who == "circle")
			{
				_grid->Cells[2][_curr_row] = "X0="+FormatFloat("00.000",results[0])+" Y0="+FormatFloat("00.000",results[1])+" R="+FormatFloat("00.000",results[2]);
			}
			if(who == "ellipse")
			{
				//debug
				String s= "X0="+FormatFloat("00.000",results[0])+" Y0="+FormatFloat("00.000",results[1]) +
					" DX0="+FormatFloat("00.000",results[2]) + " DY0="+FormatFloat("00.000",results[3])+" alpha="+FormatFloat("00.000",results[4]);
				//\debug
				_grid->Cells[2][_curr_row] = s;
			}
			break;
		}
		case DELETED:
		{
			_pos = std::find(_shapes.begin(),_shapes.end(),s);		//find the shape
			DeleteShape();                                          //and delete it
		}
	}
}
//------------------------------------------------------------------------------
Shape * TableShape::GetCurrentShape() const
{
	if(_pos != NULL)
		return *_pos;
	else return NULL;
}

//------------------------------------------------------------------------------
void TableShape::SetSelectStatus(bool status)
{
	IsSelectStatus = status;
	if(IsSelectStatus)
    {
		for(int n=0; n < ItemsSelected.size(); n++)
        {
			ItemsSelected[n] = false;
		}
	}
	_grid->Repaint();
}

//------------------------------------------------------------------------------
void TableShape::GetSelected(std::vector<Shape *> & selected_shapes)
{
	std::list<Shape *>::iterator begin = _shapes.begin(), end = _shapes.end(), item;
	int num = 0;
	for(item = begin; item != end; ++item)
	{
		if(ItemsSelected[num])
		{
			selected_shapes.push_back(*item);
		}
		num++;
	}
}
//------------------------------------------------------------------------------
void TableShape::ResetAll()
{
	while(_pos!=NULL)
	{
		_pos=_shapes.begin();
		DeleteShape();

    }
}
//------------------------------------------------------------------------------
void TableShape::DeleteShape(Shape * s)
{
	*_pos = s;
	DeleteShape();
}
void TableShape::DeleteShape()
{
 	if(_pos == NULL) return;
	// delete current shape
	delete (*_pos);
	// change pointer to current shape
	if(_pos == _shapes.begin())
	{
		if(_shapes.size() == 1)
		{
			_shapes.erase(_pos);
			_pos = NULL;
		}
		else
		{
			_shapes.erase(_pos++);
		}
	}
	else
	{
		_shapes.erase(_pos--);
	}

	for(unsigned int n = _curr_row; n < _count_row; n++)
	{
		_grid->Cells[0][n] = UIntToStr(n);
		_grid->Cells[1][n] = _grid->Cells[1][n+1];
		_grid->Cells[2][n] = _grid->Cells[2][n+1];
		_grid->Cells[3][n] = _grid->Cells[3][n+1];
	}

	if(_count_row > 1)
	{
		_grid->RowCount = _count_row;
//		if(_curr_row == _count_row)
		_curr_row--;
		_count_row--;
		if(_grid->Row>1)
		{
			_grid->Row = --_curr_row;
		}
	}
	else
	{
		_grid->Cells[0][_count_row] = "";
		_grid->Cells[1][_count_row] = "";
		_grid->Cells[2][_count_row] = "";
		_count_row--;
	}
	_grid->OnClick(_grid);
}
//------------------------------------------------------------------------------
void TableShape::GetAll(std::list<Shape*> * shapes)
{
	*shapes = _shapes;
}
//------------------------------------------------------------------------------
void TableShape::first()
{
	_curr_row=_grid->FixedRows;
}
//------------------------------------------------------------------------------
void TableShape::next()
{
	_curr_row++;
}
//------------------------------------------------------------------------------
void TableShape::last()
{
	_curr_row=_grid->RowCount;
}
//------------------------------------------------------------------------------
void TableShape::Show()
{
	std::list<Shape *>::iterator it;
	for(it = _shapes.begin(); it != _shapes.end(); it++)
	{
		(*it)->Show();
	}
}
//------------------------------------------------------------------------------
void TableShape::Hide()
{
	std::list<Shape *>::iterator it;
	for(it = _shapes.begin(); it != _shapes.end(); it++)
	{
		(*it)->Hide();
	}
}
//------------------------------------------------------------------------------
void TableShape::ShowClicked(bool TrueFalse)
{
	_showClicked = TrueFalse;
	if(TrueFalse == true)            //if this option is activated
	{
		Hide();						//first hide all the shapes
		(*_pos)->Show();    		//then show the current one
	}
	else
	{
		_visible?Show():Hide();    //if deactivated - switch to the normal mode
	}
}
//------------------------------------------------------------------------------
// TableCalculation
//------------------------------------------------------------------------------
TableCalculation::TableCalculation(Graph * graph, TForm* Owner, TStringGrid * grid)
		: GraphObserver(graph, Owner), _count_row(0)
{
//	grid.reset<TStringGrid>(new TStringGrid(owner));
//	grid->Parent = owner;
	_grid=grid;
	// Grid events
	_grid->OnDrawCell = GridDrawCell;
	_grid->OnClick = GridClick;
	_grid->OnKeyUp = GridKeyUp;
	_grid->OnMouseUp = GridMouseUp;
	_auxVisible = false;
	_marksVisible = false;

}
//------------------------------------------------------------------------------
void __fastcall TableCalculation::GridClick(TObject *Sender)
{
	if(_pos == NULL) return;

	(*_pos)->RemoveSelection();

	_curr_row = _grid->Row;
	size_t row = _grid->FixedRows;
	_pos = _calculations.begin();
	while (row < _curr_row && _pos != _calculations.end())
    {
		row++;
		_pos++;
	}

	if(_pos == _calculations.end()) return;

	(*_pos)->MakeSelection(_auxVisible,_marksVisible);
}

//------------------------------------------------------------------------------
 void __fastcall TableCalculation::GridKeyUp(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if(Key == VK_DELETE)
	{
		DeleteCalculation();
	}
}
//------------------------------------------------------------------------------
void __fastcall TableCalculation::GridDrawCell(TObject *Sender, int ACol, int ARow, const TRect &Rect,
	TGridDrawState State)
{
	if ((ACol == 0) || (ARow == 0))
		return;

}
//------------------------------------------------------------------------------
void __fastcall TableCalculation::GridMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
	int X, int Y)
{
	if(IsSelectStatus)
    {
		int col, row;
		_grid->MouseToCell(X, Y, col, row);
		if ((row > 0) && (row <= _grid->RowCount - 1))
        {
			if (ItemsSelected[_grid->Row - 1])
				ItemsSelected[_grid->Row - 1] = false;
			else
				ItemsSelected[_grid->Row - 1] = true;
		}
		_grid->Invalidate();
	}
}
//------------------------------------------------------------------------------
void TableCalculation::update(GRAPH_EVENT e, Calculation * c)
{
	std::vector<std::string> names;
	String shapenames;

	switch(e)
    {
		case CREATED:
		{
			String name = c->GetName().c_str();
			c->GetNames(&names);			//get names of shapes in calculation
			size_t sz=names.size();			//for optimizing
			for(int i=0; i<sz; i++)
			{
				shapenames+=names.at(i).c_str();
				if(i<sz-1)      			//add comma if it isn't the last shape
				{
					shapenames+=", ";
				}
			}
			if(_pos != NULL)
			{
				(*_pos)->RemoveSelection();
			}
			_calculations.push_back(c);
			_pos = _calculations.end();
			_pos--;
			(*_pos)->MakeSelection(_auxVisible,_marksVisible);

			_grid->RowCount = ++_count_row + 1;
			_curr_row = _count_row;

			_grid->Cells[0][_curr_row] = _curr_row;
			_grid->Cells[1][_curr_row] = name;
			_grid->Cells[2][_curr_row] = shapenames;
			_grid->Row = _curr_row;
			break;
		}
		case UPDATE:
		{
			String name = c->GetName().c_str();
			c->GetNames(&names);			//get names of shapes in calculation
			size_t sz=names.size();			//for optimizing
			for(int i=0; i<sz; i++)
			{
				shapenames+=names.at(i).c_str();
				if(i<sz-1)      			//add comma if it isn't the last shape
				{
					shapenames+=", ";
				}
			}
			double result = c->GetResult();

			_grid->Cells[0][_curr_row] = _curr_row;
			_grid->Cells[1][_curr_row] = name;
			_grid->Cells[2][_curr_row] = shapenames;
			_grid->Cells[3][_curr_row] = FormatFloat("00.000",result);
			break;
		}
		case DELETED:
		{
			_pos = std::find(_calculations.begin(),_calculations.end(),c);	//find the calculation
			DeleteCalculation();                                          	//and delete it
		}
	}
}

//------------------------------------------------------------------------------
Calculation * TableCalculation::GetCurrentCalculation() const
{
	if(_pos != NULL)
		return *_pos;
	else return NULL;
}

//------------------------------------------------------------------------------
void TableCalculation::SetSelectStatus(bool status)
{
	IsSelectStatus = status;
	if(IsSelectStatus)
    {
		for(int n=0; n < 100; n++)
        {
			ItemsSelected[n] = false;
		}
	}
	_grid->Repaint();
}

//------------------------------------------------------------------------------
void TableCalculation::GetSelected(std::vector<Calculation *> & selected_calculations)
{
	std::list<Calculation *>::iterator begin = _calculations.begin(), end = _calculations.end(), item;
	int num = 0;
	for(item = begin; item != end; ++item)
	{
		if(ItemsSelected[num])
		{
			selected_calculations.push_back(*item);
		}
		num++;
	}
}
//------------------------------------------------------------------------------
void TableCalculation::ResetAll()
{
	while(_pos!=NULL)
	{
		_pos=_calculations.begin();
		DeleteCalculation();

	}
}
//------------------------------------------------------------------------------
void TableCalculation::DeleteCalculation()
{
	if(_pos == NULL) return;
	// delete current calculation
	delete (*_pos);
	// change pointer to current calculation
	if(_pos == _calculations.begin())
	{
		if(_calculations.size() == 1)
		{
			_calculations.erase(_pos);
			_pos = NULL;
		}
		else
		{
			_calculations.erase(_pos++);
		}
	}
	else
	{
		_calculations.erase(_pos--);
	}

	for(unsigned int n = _curr_row; n < _count_row; n++)
	{
		_grid->Cells[0][n] = UIntToStr(n);
		_grid->Cells[1][n] = _grid->Cells[1][n+1];
		_grid->Cells[2][n] = _grid->Cells[2][n+1];
		_grid->Cells[3][n] = _grid->Cells[3][n+1];
	}

	if(_count_row > 1)
	{
		_grid->RowCount = _count_row;
//		if(_curr_row == _count_row)
		_curr_row--;
		_count_row--;
		if(_grid->Row>1)
		{
			_grid->Row = --_curr_row;
		}
	}
	else
	{
		_grid->Cells[0][_count_row] = "";
		_grid->Cells[1][_count_row] = "";
		_grid->Cells[2][_count_row] = "";
		_grid->Cells[3][_count_row] = "";
		_count_row--;
	}
	_grid->OnClick(_grid);
}
//------------------------------------------------------------------------------
void TableCalculation::GetAll(std::list<Calculation*> * calculations)
{
	*calculations = _calculations;
}
//------------------------------------------------------------------------------
void TableCalculation::first()
{
	_curr_row=_grid->FixedRows;
}
//------------------------------------------------------------------------------
void TableCalculation::next()
{
	_curr_row++;
}
//------------------------------------------------------------------------------
void TableCalculation::last()
{
	_curr_row=_grid->RowCount;
}
//------------------------------------------------------------------------------
void TableCalculation::ShowAux()
{
	_auxVisible=true;
	if(_pos!=NULL)
	{
		(*_pos)->ShowAux();
	}
}
//------------------------------------------------------------------------------
void TableCalculation::HideAux()
{
	_auxVisible=false;
	if(_pos!=NULL)
	{
		(*_pos)->HideAux();
	}
}
//------------------------------------------------------------------------------
void TableCalculation::ShowMarks()
{
	_marksVisible=true;
 	if(_pos!=NULL)
	{
		(*_pos)->ShowMarks();
	}
}
//------------------------------------------------------------------------------
void TableCalculation::HideMarks()
{
	_marksVisible=false;
	if(_pos!=NULL)
	{
		(*_pos)->HideMarks();
	}
}
#pragma package(smart_init)

