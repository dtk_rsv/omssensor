#ifndef pointH
#define pointH
#include <math.h>	//sqrt
//http://algolist.manual.ru/maths/geom/datastruct.php
//#include "Edge.h"
class Point2D
{
public:
	enum {LEFT,  RIGHT,  BEYOND,  BEHIND, BETWEEN, ORIGIN, DESTINATION};
	Point2D() : x(0.0), y(0.0) {}
	Point2D(double x_coord, double y_coord) : x(x_coord), y(y_coord) {}
	void SetX(double value) { x = value; }
	void SetY(double value) { y = value; }
	double GetX() const { return x; }
	double GetY() const { return y; }
	void Reset() { x = 0.0; y = 0.0; }
	double x;
	double y;

	Point2D operator + (Point2D&);
	Point2D operator - (Point2D&);
	Point2D friend operator * (double,Point2D&);

	double operator[] (int);

	bool operator == (Point2D&);
	bool operator != (Point2D&);

	bool operator < (Point2D&);
	bool operator > (Point2D&);

	int Classify(Point2D&, Point2D&);
	double Length();


};
#endif
