//---------------------------------------------------------------------------
#pragma hdrstop
#include "Measurements.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
/*!
 * \brief Create new measurement
 *
 * \param[in] data Pointer to RawData with data to measure
 * \param[in] graph Pointer to Graph to visualize measurements
 */
TMeasurement::TMeasurement(RawData * data, Graph * graph): _data(data), _graph(graph)
{

}
//---------------------------------------------------------------------------
/*!
 *  \brief Make measurement of one high-level paremeter(e.g. groove parameters)
 */
double TMeasurement::MakeMeasurement(int MeasureType, vector <sMeasureParams> params, String * ShapeStr)
{
	Region *r;
	Shape *s;
	vector <sMeasureParams>::iterator ParSet;
	for(ParSet=params.begin(); ParSet < params.end(); ParSet++)
	{
		CreateSpecifiedRegion((Region::REGIONTYPE)ParSet->RegionType,ParSet->RegionStr);
    }
}
//---------------------------------------------------------------------------
/*!
 * \brief Create new region by clicking specified points on chart
 *
 * \param[in] RegionType Type of region to be created
 * \param[in] Regions (optional) Regions, that will be united to multiregion
 * \return In case of successfull creation return true, otherwise false
 */
bool TMeasurement::CreateRegion(Region::REGIONTYPE RegionType, vector <Region*> Regions)
{
	GRAPH_MODE mode = _graph->GetMode();
	Region * new_region = NULL;

	if(mode == GRAPH_MODE::CREATE_REGION)
	{
		_graph->DeleteActiveRegion();
	}

	try
	{
		if(_data->GetStatus())
		{
			switch(RegionType)
			{
				case Region::FRAME:
					new_region = new Frame(_data, _graph);
					break;
				case Region::TRIANGLE:
					new_region = new Triangle(_data, _graph);
					break;
				case Region::POLYGON:
					new_region = new FreeForm(_data, _graph);
					break;
				case Region::UNION:
					new_region = new MultiRegion(_data, _graph, Regions);
					_graph->NewRegionCreated(new_region);
					return true;
			}
			if(new_region == NULL)
			{
				return false;
            }

			if(_graph->SetActiveRegion(new_region))
			{
				_graph->SetMode(GRAPH_MODE::CREATE_REGION,0);
			}
		}
		else
		{
            return false;
        }
	}
	catch (...)
	{
		//todo:error processing
		return false;
	}
	return true;
}
//----------------------------------------------------------------------
/*!
 * \brief Find and create new shape at the choosed region
 *
 * \param[in] ShapeType Type of shape to be created
 * \param[in] R Pointer to the region in which the shape will be found
 * \return In case of successfull creation return true, otherwise false
 */
bool TMeasurement::CreateShape(Shape::SHAPETYPE ShapeType, Region * R)
{
	bool result;
	Shape * new_shape;
	if(R)
	{
		switch(ShapeType)
		{
			case Shape::LINE:
				new_shape = new TLine(R, _graph);		//create new instance of line
				break;
			case Shape::CIRCLE:
				new_shape = new TCircle(R, _graph);  	//create new instance of circle
				break;
			case Shape::ELLIPSE:
				new_shape = new TEllipse(R, _graph);	//create new instance of ellipse
				break;
		}
		if(new_shape == NULL)
			return false;

		result = new_shape->Calculate(); 	//now calculate the shape
		if(result)
		{
			new_shape->Assign();              //draw the shape
		}
		return result;
	}
	else
	{
		return false;
	}
}
//----------------------------------------------------------------------
/*!
 * \brief Make required calculation with some of existing shapes
 *
 * \param[in] CalcType Type of required calculation
 * \param[in] shapes Collection of shapes for calculation
 * \return Calculated value
 */
double TMeasurement::Calculate(Calculation::CALCTYPE CalcType, vector <Shape*> shapes)
{
	double result=0;

	if(!shapes.empty())
	{
		Calculation * new_calc = NULL;
		switch(CalcType)
		{
			case Calculation::ANGLE_BETWEEN_LINES:
				new_calc = new AngleBetweenLines(_graph,shapes);
				break;
			case Calculation::DISTANCE_BETWEEN_CENTERS:
				new_calc = new DistanceBetweenCenters(_graph,shapes);
				break;
			case Calculation::DISTANCE_BETWEEN_LINES:
				new_calc = new DistanceBetweenLines(_graph,shapes);
		}

		if(new_calc == NULL)
		{
			return 0;
		}

		bool status = new_calc->Calculate();
	}
	return result;
}
//----------------------------------------------------------------------
/*!
 * \brief Create region by given type and parameters
 * \param[in] RegionType Type of required region
 * \param[in] RegionStr String with parameters of the region
 */
void TMeasurement::CreateSpecifiedRegion(Region::REGIONTYPE RegionType, String RegionStr)
{
	//todo
//	GRAPH_MODE mode = _graph->GetMode();
//	if(mode == GRAPH_MODE::CREATE_REGION)
//	{
//		_graph->DeleteActiveRegion();
//	}
//
//	if(_data->GetStatus())
//	{
//		Region * new_region = NULL;
//		enum { FRAME, TRIANGLE };
//		switch(RegionType)
//		{
//			case FRAME:
//				new_region = new Frame(_data, _graph);
//				break;
//			case TRIANGLE:
//				new_region = new Triangle(_data, _graph);
//				break;
//		}
//		if(new_region == NULL)
//			return;
//
//		//todo:here we must create new region according with RegionStr
//		if(_graph->SetActiveRegion(new_region))
//		{
//			_graph->SetMode(GRAPH_MODE::CREATE_REGION,0);
//		}
//	}
}
//----------------------------------------------------------------------
/*!
 * \brief Calculate all shapes and calculations
 */
void TMeasurement::CalculateAll(TableCalculation * c, TableShape * s)
{
	std::list<Calculation*> existedCalculations;     		//all the calculations
	vector <Shape*> shapesInCalc;

	//todo:move all recalculations into thread and visualizatiom methods here

	CalculateShapes(s);					//re-approximate shapes

	c->GetAll(&existedCalculations); 	//get all calculations
	c->first();
	for(list<Calculation*>::iterator it = existedCalculations.begin(); it!=existedCalculations.end(); it++)
	{
		Calculation * calculation = *it;
		calculation->Calculate();		//looks bad :) but here we re-calculate our measurement
		c->next();
	}

}
//----------------------------------------------------------------------
/*!
 * \brief Recalculate all the shapes
 * \param[in] s Pointer to TableShape table with shape parameters
 */
void TMeasurement::CalculateShapes(TableShape * s)
{
	list <Shape*> existedShapes;     		//all the shapes

	//todo:move all recalculations into thread and visualizatiom methods here

s->GetAll(&existedShapes); 	//get all shapes for all regions

	s->first();    				//switch to the first row in the shapes grid
	for(list<Shape*>::iterator it=existedShapes.begin();it!=existedShapes.end();it++)
	{
		Shape * shape = *it;   	//re-approximate all the shapes
		shape->Calculate();
        shape->Update();
		s->next();
	}
}
