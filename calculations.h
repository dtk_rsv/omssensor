#ifndef calculationsH
#define calculationsH
#include <vector>
#include <iterator>
#include <math.h>
#include "thrlog.h"
#include "shapes.h"
class Calculation
{
	public:
		enum CALCTYPE{ANGLE_BETWEEN_LINES, DISTANCE_BETWEEN_CENTERS, DISTANCE_BETWEEN_LINES };

		Calculation(Graph* g, std::vector<Shape *> s, std::string n);
		virtual ~Calculation() = 0;

		virtual bool Calculate() = 0;
		virtual double GetResult() { return result; }
        virtual void GetNames(std::vector <std::string> * names);
		size_t GetSize() const { return shapes.size(); }
		std::string GetName() const {return name;};
		void GetShapes(std::vector<Shape*> * s);
		void MakeSelection(bool AuxVisible, bool MarksVisible);
		void RemoveSelection();
		void ShowAux();
		void HideAux();
		void ShowMarks();
		void HideMarks();

		double CalcPerpendicular(Point2D pLine1, Point2D pLine2, Point2D pOrigin, Point2D * pDest);
		double CalcLength(Point2D p1, Point2D p2);
		double CalcLineEquation(Point2D p1, Point2D p2, double * K, double * B);
		Point2D CalcCrossPoint(Point2D pL1_1, Point2D pL1_2,Point2D pL2_1, Point2D pL2_2);



	protected:
		Graph * graph;
		std::vector<Shape *> shapes;
		std::vector<TLineSeries*> series;
		std::string name;
		double result;

	private:
};

class AngleBetweenLines : public Calculation
{
	public:
		AngleBetweenLines(Graph* g, std::vector<Shape *> s);
		virtual bool Calculate();
		~AngleBetweenLines();

	private:
		static const unsigned int _count_shapes = 2;
		TLineSeries * _sL1;   	//full line 1
		TLineSeries * _sL2;     //full line 2
};

class DistanceBetweenCenters : public Calculation
{
	public:
		DistanceBetweenCenters(Graph* g, std::vector<Shape *> s);
		virtual bool Calculate();
		~DistanceBetweenCenters();

	private:
		static const unsigned int _count_shapes = 2;

		TLineSeries * _sDist;	//series from 1st center to the 2nd one
};

class DistanceBetweenLines : public Calculation
{
	public:
		DistanceBetweenLines(Graph *g, std::vector<Shape *> s);
		~DistanceBetweenLines();
		virtual bool Calculate();

	private:
		static const unsigned int _count_shapes = 2;

		TLineSeries * sB;   	//series for bisector
		TLineSeries * sp[6];    //series for 6 perpendiculars
		TLineSeries * sL1;      //series for line 1
		TLineSeries * sL2;      //series for line 2
};

#endif
