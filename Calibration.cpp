#include "Calibration.h"
//------------------------------------------------------------------------------
Calibration::Calibration()
{
	for(int numCoeff = MinLevel; numCoeff < MaxLevel; numCoeff++)
		_poly9[numCoeff]=0;
	for(int nRow = MinLevel; nRow < MaxLevelMatrix; nRow++)
	{
		for(int nCol = MinLevel; nCol < MaxLevelMatrix; nCol++)
		{
			_polyMatrix[nRow][nCol]=0;
		}
	}
	_mean = 0;
	_std = 0;
	_offset = 0;
	_reflection = 0;
}
//------------------------------------------------------------------------------
bool Calibration::Init(const std::string filename)
{
	TIniFile * ini = new TIniFile(filename.c_str());
	if(!ini)	return false;

	for(int numCoeff = MinLevel; numCoeff < MaxLevel; numCoeff++)
	{
		_poly9[numCoeff]=ini->ReadFloat("Poly9","p"+IntToStr(numCoeff),0);
	}
	for(int nRow = MinLevel; nRow < MaxLevelMatrix; nRow++)
	{
		for(int nCol = MinLevel; nCol < MaxLevelMatrix; nCol++)
		{
			_polyMatrix[nRow][nCol]=ini->ReadFloat("Matrix","p"+IntToStr(nRow)+IntToStr(nCol),0);
		}
	}
	_mean = ini->ReadFloat("Misc","mean",0);
	_std = ini->ReadFloat("Misc","std",0);
	_meanZ = ini->ReadFloat("Misc","meanY",0);
	_stdZ = ini->ReadFloat("Misc","stdY",0);
	_offset = ini->ReadFloat("Misc","offset",0);
	_reflection = ini->ReadFloat("Misc","reflection",0);
	delete ini;
	return true;
}
//------------------------------------------------------------------------------
double Calibration::GetPoly9Coeff(unsigned int num)const
{
	if(num>=MinLevel && num < MaxLevel)
		return _poly9[num];
	else return -32767;
}

//------------------------------------------------------------------------------
//double Calibration::GetPoly2Coeff(unsigned int num)const
//{
//	if(num>=MinLevel && num < MaxLevelK)
//		return _poly2[num];
//	else return -32767;
//}
//------------------------------------------------------------------------------
/*!
 * \brief 9-degree polynomial approximation
 *
 * f(x) = p0*x^9 + p1*x^8 + p2*x ^7+ p3*x^6 + p4*x^5 + p5*x^4 + p6*x^3 + p7*x^2 + p8*x + p9
 * \param[in] cur_val input value
 * \return approximated value or 0xDEADBEEF if the calculation didn't succeed
 */
double Calibration::Poly9(const double cur_val)
{
	double result=0.0;

	try
	{
		result =	_poly9[0] * pow(cur_val, 9) +
					_poly9[1] * pow(cur_val, 8) +
					_poly9[2] * pow(cur_val, 7) +
					_poly9[3] * pow(cur_val, 6) +
					_poly9[4] * pow(cur_val, 5) +
					_poly9[5] * pow(cur_val, 4) +
					_poly9[6] * pow(cur_val, 3) +
					_poly9[7] * pow(cur_val, 2) +
					_poly9[8] * cur_val +
					_poly9[9];
	}
	catch(Exception &e)
	{
		throw(e);
		return 0xDEADBEEF;
	}
	return result;
}
//------------------------------------------------------------------------------
/*!
 * \brief 2-degree polynomial approximation
 *
 * f(x) = p0*x^2 + p1*x + p2
 * \param[in] cur_val input value
 * \return approximated value or 0xDEADBEEF if the calculation didn't succeed
 */
//double Calibration::Poly2(const double cur_val)
//{
//	double result=0.0;
//
//	try
//	{
//		result =	_poly2[0] * pow(cur_val, 2) +
//					_poly2[1] * cur_val +
//					_poly2[2];
//	}
//	catch(Exception &e)
//	{
//		throw(e);
//		return 0xDEADBEEF;
//	}
//	return result;
//}
//------------------------------------------------------------------------------
/*!
 * \brief 1-degree polynomial matrix approximation
 *
 * // f(x,y) = p00+p10x+p01y
 * \param[in] cur_x raw x
 * \param[out] cur_y raw y
 * \return approximated x or 0xDEADBEEF if the calculation didn't succeed
 */
double Calibration::Matrix(const double cur_x, const double cur_y)
{
	double result=0.0;

	try
	{
		result = _polyMatrix[0][0] + _polyMatrix[1][0]*cur_x + _polyMatrix[0][1]*cur_y + _polyMatrix[2][0]*cur_x*cur_x + _polyMatrix[1][1]*cur_x*cur_y+ _polyMatrix[0][2]*cur_y*cur_y;
	}
	catch(Exception &e)
	{
		throw(e);
		return 0xDEADBEEF;
	}
	return result;
}

