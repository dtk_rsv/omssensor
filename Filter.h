#ifndef FilterH
#define FilterH
#include <deque>
#include <vector>
#include <algorithm>

class Filter
{
public:
	Filter(size_t wnd_sz);
	void Set(double value);
	double Get() const;
	std::string GetStr() const;

private:
	size_t wnd_size;
	std::deque<double> data;
};
#endif
