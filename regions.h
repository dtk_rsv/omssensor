#ifndef regionsH
#define regionsH

#include <Math.hpp>		// SimpleRoundTo
#include <vector>
#include <algorithm>

//#include "main.h"
#include "data.h"
#include "graph.h"
#include "point.h"


//------------------------------------------------------------------------------
class Region
{
public:
	enum REGIONTYPE { FRAME, TRIANGLE, POLYGON, UNION };
	Region(RawData * d, Graph * g, const std::string & name, const unsigned int & type);
	virtual ~Region();

	virtual void SetVertex(const double & x, const double & y) = 0;
	virtual void MouseDown(const int & x, const int & y) = 0;
	virtual void MouseMove(const int & x, const int & y) = 0;
	virtual void KeyDown(WORD k) = 0;
	virtual size_t GetPointsInside(std::vector<Point2D> & points) = 0;
	virtual size_t GetPointsInside(std::vector<double> & vec_x, std::vector<double> & vec_y) = 0;
	virtual void GetRegionBoundariesX(double & left, double & right) = 0;


	void GetDataBoundariesX(double & left, double & right);

	std::string GetName() const  { return name; }
	unsigned int GetType() const { return type; }

	virtual void MakeSelection(bool ShowNeeded) = 0;		//these methods are virtual
	virtual void RemoveSelection(bool HideNeeded) = 0;      //because they are different for
	virtual void Show() = 0;                                //a multiregion
	virtual void Hide() = 0;

	std::vector <Shape* > myShapes;

protected:
	RawData * data;
	Graph * graph;
	int count;
	TLineSeries * series;

private:
	std::string name;
	unsigned int type;

};

//------------------------------------------------------------------------------
class Frame : public Region
{
public:
	Frame(RawData * d, Graph * g);
	virtual ~Frame();
	virtual void SetVertex(const double & x, const double & y);
	virtual void MouseDown(const int & x, const int & y);
	virtual void MouseMove(const int & x, const int & y);
	virtual void KeyDown (WORD k);
	virtual size_t GetPointsInside(std::vector<Point2D> & points);
	virtual size_t GetPointsInside(std::vector<double> & vec_x, std::vector<double> & vec_y);
	virtual void GetRegionBoundariesX(double & left, double & right);

	virtual void Show();
	virtual void Hide();
	virtual void MakeSelection(bool ShowNeeded);
	virtual void RemoveSelection(bool HideNeeded);

private:
	TColor color;
	double x1, y1;
	double x2, y2;
	bool left, right;

};

//------------------------------------------------------------------------------
class Triangle : public Region
{
public:
	Triangle(RawData * d, Graph * g);
	virtual ~Triangle();
	virtual void SetVertex(const double & x, const double & y);
	virtual void MouseDown(const int & x, const int & y);
	virtual void MouseMove(const int & x, const int & y);
	virtual void KeyDown (WORD k);
	virtual size_t GetPointsInside(std::vector<Point2D> & points);
	virtual size_t GetPointsInside(std::vector<double> & vec_x, std::vector<double> & vec_y);
	virtual void GetRegionBoundariesX(double & left, double & right);

	virtual void Show();
	virtual void Hide();
	virtual void MakeSelection(bool ShowNeeded);
	virtual void RemoveSelection(bool HideNeeded);

private:
	static const int count_vertex = 3;
	std::vector<Point2D> vertex;
	TColor color;
};
//------------------------------------------------------------------------------
class MultiRegion : public Region
{
public:
	MultiRegion(RawData * d, Graph * g, std::vector <Region*> Regions);
	virtual ~MultiRegion();
	virtual void SetVertex(const double & x, const double & y){}
	virtual void MouseDown(const int & x, const int & y){}
	virtual void MouseMove(const int & x, const int & y){}
	virtual void KeyDown (WORD k) {};
	virtual size_t GetPointsInside(std::vector<Point2D> & points);
	virtual size_t GetPointsInside(std::vector<double> & vec_x, std::vector<double> & vec_y);
	virtual void GetRegionBoundariesX(double & left, double & right);

	virtual void Show();
	virtual void Hide();
	virtual void MakeSelection(bool ShowNeeded);
	virtual void RemoveSelection(bool HideNeeded);

private:
	std::vector <Region*> _regions;
};

//------------------------------------------------------------------------------
class FreeForm : public Region
{
public:
	FreeForm(RawData * d, Graph * g);
	virtual ~FreeForm();
	virtual void SetVertex(const double & x, const double & y);
	virtual void MouseDown(const int & x, const int & y);
	virtual void MouseMove(const int & x, const int & y);
	virtual void KeyDown (WORD k);
	virtual size_t GetPointsInside(std::vector<Point2D> & points);
	virtual size_t GetPointsInside(std::vector<double> & vec_x, std::vector<double> & vec_y);
	virtual void GetRegionBoundariesX(double & left, double & right);

	virtual void Show();
	virtual void Hide();
	virtual void MakeSelection(bool ShowNeeded);
	virtual void RemoveSelection(bool HideNeeded);

	inline double _dabs(double d)
	{
		return d>0?d:d*(-1);
    }

private:
	bool IsPointInPolygon(Point2D p);
	int count_vertex;
	std::vector<Point2D> vertex;
	TColor color;
};
#endif
