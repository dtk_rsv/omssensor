//---------------------------------------------------------------------------
#ifndef NetCommandsH
#define NetCommandsH
#include <vcl.h>
#include <list>
using namespace std;
//---------------------------------------------------------------------------
class NetCmd
{
	public:
	NetCmd(String CmdName, int Addr, int Mask, int Value);
	NetCmd(String CmdName, String Addr, String Mask, String Value);
    NetCmd(String Cmd);
	String cmdName;
	int address;
	int mask;
	int value;

	String sRequest;
	String sResponse;
};
#endif
