#pragma hdrstop
#include "point.h"

#pragma package(smart_init)
Point2D Point2D::operator + (Point2D &p)
{
	return Point2D (x + p.x, y + p.y);
}

Point2D Point2D::operator - (Point2D &p)
{
	return Point2D (x - p.x, y - p.y);
}

Point2D operator * (double s , Point2D &p)
{
	return Point2D (s * p.x, s * p.y);
}

double Point2D::operator [] (int i)
{
  return (i == 0) ? x : y;
}

bool Point2D::operator == (Point2D &p)
{
	return (x == p.x) && (y == p.y);
}

bool Point2D::operator != (Point2D &p)
{
	return !(*this == p);
}

bool Point2D::operator < (Point2D &p)
{
	return ((x < p.x) || ((x == p.x) && (y<p.y)));
}

bool Point2D::operator > (Point2D &p)
{
	return ((x>p.x) || ((x == p.x) && (y > p.y)));
}

int Point2D::Classify(Point2D &p0, Point2D &p1)
{
	Point2D p2 = *this;
	Point2D a = p1 - p0;
	Point2D b = p2 - p0;
	double sa = a. x * b.y - b.x * a.y;
	if (sa > 0.0)
		return LEFT;
	if (sa < 0.0)
		return RIGHT;
	if ((a.x * b.x < 0.0) || (a.y * b.y < 0.0))
		return BEHIND;
	if (a.Length() < b.Length())
		return BEYOND;
	if (p0 == p2)
		return ORIGIN;
	if (p1 == p2)
		return DESTINATION;
 	 return BETWEEN;
}
double Point2D::Length (void)
{
  return sqrt(x*x + y*y);
}
