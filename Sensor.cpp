//---------------------------------------------------------------------------

#pragma hdrstop

#include "Sensor.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
		void TOMSSensor::Init(TForm * Form, TDBChart * Chart,
			TStringGrid * sgRegions, TStringGrid * sgShapes, TStringGrid * sgCalculations,
		CallbackFrame_t CallbackFrame, CallbackVideo_t CallbackVideo, CallbackException_t CallbackExc)
		{
			CreateDirectory(L".\\Log\\",NULL);
			log = new Log(".\\Log\\View.log",1);
			TIniFile * ini = new TIniFile (".\\config.ini");
//			sgRegions->ColWidths[0]=25;
//			sgRegions->ColWidths[1]=58;
//			sgRegions->ColWidths[2]=210;
//			sgRegions->Cells[0][0]="�";
//			sgRegions->Cells[1][0]="���";
//			sgRegions->Cells[2][0]="���������";
//
//			sgShapes->ColWidths[0]=25;
//			sgShapes->ColWidths[1]=58;
//			sgShapes->Cells[0][0]="�";
//			sgShapes->Cells[1][0]="���";
//			sgShapes->Cells[2][0]="���������";
//			sgShapes->ColWidths[2]=210;
//
//			sgCalculations->ColWidths[0]=25;
//			sgCalculations->ColWidths[1]=110;
//			sgCalculations->ColWidths[2]=90;
//			sgCalculations->ColWidths[3]=70;
//			sgCalculations->Cells[0][0]="�";
//			sgCalculations->Cells[1][0]="���";
//			sgCalculations->Cells[2][0]="������";
//			sgCalculations->Cells[3][0]="���������";

			graph = new Graph(Chart);
			regions = new TableRegion(graph, Form, sgRegions);
			shapes = new TableShape(graph, Form, sgShapes);
			calculations = new TableCalculation (graph, Form, sgCalculations);
			data = new RawData();

			series = new TPointSeries(graph->GetContext());
			series->ParentChart = graph->GetContext();
			series->SeriesColor = clBlack;
			series->XValues->Order = loNone;
			series->YValues->Order = loNone;
			series->Legend->Visible = false;

			series->Pointer->Visible = true;
			series->Pointer->Style = psCircle;
			series->Pointer->VertSize = 1;
			series->Pointer->HorizSize = 1;

			graph->GetContext()->Axes->Bottom->Maximum = ini->ReadInteger("ChartSize","width",800);
			graph->GetContext()->Axes->Left->Maximum = ini->ReadInteger("ChartSize","height",600);


			thrDataStream = new TDataStream(true,*CallbackFrame,*CallbackVideo,*CallbackExc);

		}
