#pragma hdrstop
//#include "main.h"
#include "graph.h"
#include "regions.h"
#include "shapes.h"
#include "calculations.h"
#include "graph_observers.h"

//------------------------------------------------------------------------------
Graph::Graph(TDBChart * context) : draw_context(context), mode(INDICATE)
{
	log = new Log(".\\Log\\Model.log",1);
	draw_context->OnMouseDown = GraphMouseDown;
	draw_context->OnMouseUp = GraphMouseUp;
	draw_context->OnMouseMove = GraphMouseMove;
	draw_context->OnKeyDown = GraphKeyDown;

	size_t point_size = 2;
	_series = new TPointSeries(draw_context);
	_series->ParentChart = draw_context;
	_series->SeriesColor = clBlack;
	_series->XValues->Order = loNone;
	_series->YValues->Order = loNone;

	_series->Legend->Visible = false;

	_series->Pointer->Visible = true;
	_series->Pointer->Style = psCircle;
	_series->Pointer->VertSize = point_size;
	_series->Pointer->HorizSize = point_size;

}

//------------------------------------------------------------------------------
void Graph::AddData(RawData * d)
{
	_series->Clear();

	int size = d->GetSize();
	for(int num = 0; num < size; ++num)
	{
		Point2D p = d->GetPoint(num);
		_series->AddXY(p.GetX(), p.GetY());
	}
}

//------------------------------------------------------------------------------
bool Graph::SetActiveRegion(Region * r)
{
	std::list<Region*>::iterator pos = find(regions.begin(), regions.end(), r);
	if(pos == regions.end())
	{
		active_region = NULL;
		return false;
	}
	else
	{
		active_region = *pos;
		return true;
	}
}

//------------------------------------------------------------------------------
void Graph::DeleteActiveRegion()
{
	delete active_region;
	active_region = NULL;
}
//------------------------------------------------------------------------------
void Graph::DeactivateRegion()
{
	active_region = NULL;
	mode = INDICATE;
}

//------------------------------------------------------------------------------
void Graph::DeleteAllRegions()
{
	std::list<Region*>::iterator begin = regions.begin(),
		end = regions.end(), pos;
	for(pos = begin; pos != end; ++pos)
		delete *pos;
}

//------------------------------------------------------------------------------
void __fastcall Graph::GraphMouseDown(TObject *Sender, TMouseButton Button,
	TShiftState Shift, int X, int Y)
{
	if(mode == CREATE_REGION)
	{
		active_region->MouseDown(X, Y);
	}

	if(mode == INDICATE)
	{

	}
}
//------------------------------------------------------------------------------
void __fastcall Graph::GraphMouseUp(TObject *Sender, TMouseButton Button,
	TShiftState Shift, int X, int Y)
{
//
}
//------------------------------------------------------------------------------
void __fastcall Graph::GraphMouseMove(TObject *Sender, TShiftState Shift, int X, int Y)
{
	if(mode == CREATE_REGION)
	{
		active_region->MouseMove(X, Y);
	}

	if(mode == INDICATE)
	{

	}
}
//------------------------------------------------------------------------------
void __fastcall Graph::GraphKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if(mode != INDICATE)
	{
		active_region->KeyDown(Key);
	}
}

//------------------------------------------------------------------------------
void Graph::NewRegionCreated(Region * r)
{
	mode = INDICATE;
	active_region = NULL;
	notify(GRAPH_EVENT::CREATED, r);
}
//------------------------------------------------------------------------------
// Note! class Region call this method in own destructor
void Graph::DeleteRegion(Region * r)
{
	regions.remove(r);
}
//------------------------------------------------------------------------------
// Note! class Shape call this method in own destructor
void Graph::DeleteShape(Shape * s)
{
	shapes.remove(s);
}

//------------------------------------------------------------------------------
void Graph::NewShapeCreated(Shape * s)
{
	notify(GRAPH_EVENT::CREATED, s);
}

//------------------------------------------------------------------------------
// Observer methods
//------------------------------------------------------------------------------
// Atach observers
void Graph::attach(GraphObserver *obs)
{
	observers.push_back(obs);
}

// Detach observers
void Graph::detach()
{
	observers.clear();
}

// Notify observers
void Graph::notify(GRAPH_EVENT e, Region * r)
{
	//for(size_t i = 0; i < observers.size(); ++i)
		observers[0]->update(e, r);
}

void Graph::notify(GRAPH_EVENT e, Shape * s)
{
//	for(size_t i = 0; i < observers.size(); ++i)
		observers[1]->update(e, s);
}

void Graph::notify(GRAPH_EVENT e, Calculation * c)
{
//	for(size_t i = 0; i < observers.size(); ++i)
		observers[2]->update(e, c);
}
#pragma package(smart_init)
