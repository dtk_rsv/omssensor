#ifndef shapesH
#define shapesH

#include <vector>
#include <numeric>

#include "regions.h"
#include "graph.h"
#include "point.h"

/*!
 * \brief Basic Shape class
 */
class Shape
{
	public:
		enum SHAPETYPE {LINE,CIRCLE,ELLIPSE};
		Shape(Region * r, Graph * g, const std::string & n, int type, const TColor color);
		void Show();
		void Hide();

		void MakeSelection(bool ShowNeeded);
		void RemoveSelection(bool HideNeeded);

		virtual ~Shape();
		virtual bool Calculate() = 0;
		virtual std::string GetResults(std::vector<double> & results) = 0;

		virtual void Assign() = 0;
		virtual void Clear() = 0;
		virtual void Update() = 0;

		virtual size_t GetIndex() const = 0;
		virtual std::string GetFullName() const = 0;

		Region * GetRegionAndShapeType(int * type) const;

		std::vector<Calculation *> myCalcs;		//calculations with this shape

	protected:
		Region * region;
		Graph * graph;
		std::string name;
		int type;

		TLineSeries * _series;
		TColor _color;
};

class TCircle : public Shape
{
	public:
		TCircle(Region * r, Graph * g);
		virtual ~TCircle();
		virtual bool Calculate();
		virtual std::string GetResults(std::vector<double> & results);

		virtual void Assign();
		virtual void Clear();
		virtual void Update();

		virtual size_t GetIndex() const {return _index;}
		virtual std::string GetFullName() const;

	private:
		double _x0, _y0;
		double _R;

		size_t& Count()      		//index of created circle
		{
			static size_t c=0;
			return c;
		}
		int _index;
};

class TLine : public Shape
{
	public:
		TLine(Region * r, Graph * g);
		virtual ~TLine();
		virtual bool Calculate();
		virtual std::string GetResults(std::vector<double> & results);

		virtual void Assign();
		virtual void Clear();
		virtual void Update();

		virtual size_t GetIndex() const {return _index;}
		virtual std::string GetFullName() const;

	private:
		double _k, _b;
		double _angleX;
		double _angleY;
		double _L;
		double _x1,_x2;
		double _y1,_y2;
		size_t& Count()      		//index of created line
		{
			static size_t c=0;
			return c;
		}
		int _index;
};

class TEllipse : public Shape
{
	public:
		TEllipse(Region * r, Graph * g);
		virtual ~TEllipse();
		virtual bool Calculate();
		virtual std::string GetResults(std::vector<double> & results);

		virtual void Assign();
		virtual void Clear();
		virtual void Update();

		virtual size_t GetIndex() const {return _index;}
		virtual std::string GetFullName() const;

	private:
		double _X0;         //the center of the ellipse at X
		double _Y0;         //the center of the ellipse at Y
		double _DX0;        //small semi-axes
		double _DY0;        //big semi-axes
		double _Alpha; 		//angle of ellipse
		double _LX;         //bottom axes proection
		double _LY;         //left axes proection

		size_t& Count()    	//index of created ellipse
		{
			static size_t c=0;
			return c;
		}
		int _index;
};
#endif
