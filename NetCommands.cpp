//---------------------------------------------------------------------------

#pragma hdrstop

#include "NetCommands.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
NetCmd::NetCmd(String CmdName, int Addr, int Mask, int Value):
	cmdName(CmdName), address(Addr), mask(Mask),value(Value)
{
	sRequest = cmdName + IntToHex(address,4)+IntToHex(mask,4)+IntToHex(value,4);
};
//---------------------------------------------------------------------------
NetCmd::NetCmd(String CmdName, String Addr, String Mask, String Value)
{
	sRequest = cmdName + address + mask + value;
};

NetCmd::NetCmd(String Cmd)
{
	sRequest = Cmd;
};