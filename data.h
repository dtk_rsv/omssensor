#ifndef dataH
#define dataH
#include <string>
#include <vector>
#include <fstream>
#include <locale>
#include <iterator>
#include "point.h"

//------------------------------------------------------------------------------
class RawData
{
	public:
		RawData() : _status(false), _size(0.0){};
		bool ReadFromFile(const std::string & filename);
		bool GetStatus() const { return _status; }
		size_t GetSize() const { return _points.size(); }
		Point2D GetPoint(int num) const;

		void Assign (std::vector<Point2D> p);
		void Append (std::vector<Point2D> p);
		void SetPoint(double x, double y);
		void Clear();

	private:
		bool _status;
		int _size;
		std::vector<Point2D> _points;
};
#endif
