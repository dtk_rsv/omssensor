#pragma hdrstop

#include "regions.h"
//------------------------------------------------------------------------------
Region::Region(RawData * d, Graph * g, const std::string & name, const unsigned int & type)
	: data(d), graph(g), count(0), name(name), type(type)
{
	graph->AddRegion(this);
}
//------------------------------------------------------------------------------
Region::~Region()
{
	graph->DeleteRegion(this);
	for(int i = 0; i < myShapes.size(); i++)
	{
		Shape * s = myShapes.at(i);
		graph->notify(DELETED,s);
	}
}
//------------------------------------------------------------------------------
void Region::GetDataBoundariesX(double & left, double & right)
{
	std::vector<double> x, y;
	GetPointsInside(x, y);
	if(!x.empty() && !y.empty())
	{
		left = *(std::min_element(x.begin(), x.end()));
		right = *(std::max_element(x.begin(), x.end()));
	}
	else
	{
		left = right = 0;
	}
}

//------------------------------------------------------------------------------
// Triangle
//------------------------------------------------------------------------------
Frame::Frame(RawData * d, Graph * g) : Region(d, g, "Frame",FRAME),
	x1(0), y1(0), x2(0), y2(0), left(false), right(false), color(clRed)
{
	const size_t point_size = 3;
	series = new TLineSeries(g->GetContext());
	series->ParentChart = g->GetContext();
	series->SeriesColor = color;
	series->XValues->Order = loNone;
	series->YValues->Order = loNone;

	series->Legend->Visible = false;

	series->Pointer->Visible = true;
	series->Pointer->Style = psCircle;
	series->Pointer->VertSize = point_size;
	series->Pointer->HorizSize = point_size;
}

//------------------------------------------------------------------------------
Frame::~Frame()
{
	delete series;
}

//------------------------------------------------------------------------------
void Frame::MouseDown(const int & x, const int & y)
{
	const signed char ADigit = -3;
	if(!left) {
		left = true;
		x1 = Math::SimpleRoundTo(series->XScreenToValue(x), ADigit);
		y1 = Math::SimpleRoundTo(series->YScreenToValue(y), ADigit);
	}
	else if(!right) {
		right = true;
		x2 = Math::SimpleRoundTo(series->XScreenToValue(x), ADigit);
		y2 = Math::SimpleRoundTo(series->YScreenToValue(y), ADigit);

		if(x1 > x2)
			std::swap(x1, x2);
		if(y1 < y2)
			std::swap(y1, y2);

		graph->NewRegionCreated(this);
	}
}

//------------------------------------------------------------------------------
void Frame::MouseMove(const int & x, const int & y)
{
	const signed char ADigit = -3;
	double x2_t = SimpleRoundTo(series->XScreenToValue(x), ADigit);
	double y2_t = SimpleRoundTo(series->YScreenToValue(y), ADigit);

	series->Clear();
	if(right)
	{
		x2_t = x2;
		y2_t = y2;
	}

	if(left)
	{
		series->AddXY(x1, y1);
		series->AddXY(x2_t, y1);
		series->AddXY(x2_t, y2_t);
		series->AddXY(x1, y2_t);
		series->AddXY(x1, y1);
	}
}
//------------------------------------------------------------------------------
void Frame::KeyDown(WORD k)
{
	if (k == VK_ESCAPE)
	{
		graph->DeactivateRegion();
		series->Clear();
		count=0;
	}
}
//------------------------------------------------------------------------------
size_t Frame::GetPointsInside(std::vector<Point2D> & points)
{
	size_t size = data->GetSize();
	for(size_t num = 0; num < size; ++num)
	{
		Point2D current_point = data->GetPoint(num);

		double x = current_point.GetX();
		double y = current_point.GetY();

		if((x > x1) && (x < x2) && (y < y1) && (y > y2))
		{
			points.push_back(current_point);
		}
	}
	return points.size();
}

//------------------------------------------------------------------------------
size_t Frame::GetPointsInside(std::vector<double> & vec_x, std::vector<double> & vec_y)
{
	size_t size = data->GetSize();
	for(size_t num = 0; num < size; ++num)
	{
		Point2D current_point = data->GetPoint(num);

		double x = current_point.GetX();
		double y = current_point.GetY();

		if((x > x1) && (x < x2) && (y < y1) && (y > y2))
		{
			vec_x.push_back(x);
			vec_y.push_back(y);
		}
	}
	return vec_x.size();
}
//------------------------------------------------------------------------------
void Frame::Show()
{
	series->Visible=true;
}
//------------------------------------------------------------------------------
void Frame::Hide()
{
	series->Visible=false;
}
//------------------------------------------------------------------------------
void Frame::MakeSelection(bool ShowNeeded)
{
	series->LinePen->Width=5;
	if(ShowNeeded)
	{
		series->Visible = true;
	}
}
//------------------------------------------------------------------------------
void Frame::RemoveSelection(bool HideNeeded)
{
	series->LinePen->Width=1;
	if(HideNeeded)
	{
		series->Visible = false;
	}
}
//------------------------------------------------------------------------------
void Frame::GetRegionBoundariesX(double & left, double & right)
{
	left = x1;
	right = x2;
}

//------------------------------------------------------------------------------
void Frame::SetVertex(const double & x, const double & y)
{

}

//------------------------------------------------------------------------------
// Triangle
//------------------------------------------------------------------------------
Triangle::Triangle(RawData * d, Graph * g) :
	Region(d, g, "Triangle",TRIANGLE), color(clBlue)
{
	const size_t point_size = 3;
	series = new TLineSeries(g->GetContext());
	series->ParentChart = g->GetContext();
	series->SeriesColor = color;
	series->XValues->Order = loNone;
	series->YValues->Order = loNone;

	series->Legend->Visible = false;

	series->Pointer->Visible = true;
	series->Pointer->Style = psCircle;
	series->Pointer->VertSize = point_size;
	series->Pointer->HorizSize = point_size;
}

//------------------------------------------------------------------------------
Triangle::~Triangle()
{
	delete series;
}

//------------------------------------------------------------------------------
void Triangle::MouseDown(const int & x, const int & y)
{
	if(count == count_vertex) return;
	count++;

	const signed char ADigit = -3;
	double x1 = SimpleRoundTo(series->XScreenToValue(x), ADigit);
	double y1 = SimpleRoundTo(series->YScreenToValue(y), ADigit);
	vertex.push_back(Point2D(x1, y1));

	if(series->Count() == 0) {
		series->AddXY(x1, y1);
	}
	series->AddXY(x1, y1);
	if(count == count_vertex) {
		double x = series->XValues->operator [](0);
		double y = series->YValues->operator [](0);
		series->AddXY(x, y);
		graph->NewRegionCreated(this);
	}
}

//------------------------------------------------------------------------------
void Triangle::MouseMove(const int & x, const int & y)
{
	if(count == count_vertex) return;
	int curr_count = series->Count();
	if(curr_count != 0) {
		const signed char ADigit = -3;
		double x2 = SimpleRoundTo(series->XScreenToValue(x), ADigit);
		double y2 = SimpleRoundTo(series->YScreenToValue(y), ADigit);

		if(curr_count == count_vertex + 1) {
			series->Delete(curr_count - 1);
			curr_count = series->Count();
		}

		series->Delete(curr_count - 1);
		series->AddXY(x2, y2);

		if(count == count_vertex - 1) {
			double x = series->XValues->operator [](0);
			double y = series->YValues->operator [](0);
			series->AddXY(x, y);
		}

	}
}
//------------------------------------------------------------------------------
void Triangle::KeyDown(WORD k)
{
	if (k == VK_ESCAPE)
	{
		graph->DeactivateRegion();
		series->Clear();
		count=0;
		vertex.clear();
	}
}
//------------------------------------------------------------------------------
size_t Triangle::GetPointsInside(std::vector<Point2D> & points)
{
	size_t size = data->GetSize();

	double x1 = vertex[0].GetX();
	double y1 = vertex[0].GetY();

	double x2 = vertex[1].GetX();
	double y2 = vertex[1].GetY();

	double x3 = vertex[2].GetX();
	double y3 = vertex[2].GetY();

	for(size_t num = 0; num < size; ++num) {
		Point2D p = data->GetPoint(num);
		double x = p.GetX();
		double y = p.GetY() ;

		double a = (x1 - x) * (y2 - y1) - (x2 - x1) * (y1 - y);
		double b = (x2 - x) * (y3 - y2) - (x3 - x2) * (y2 - y);
		double c = (x3 - x) * (y1 - y3) - (x1 - x3) * (y3 - y);

		if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0)) {
			points.push_back(p);
		}
	}
	return points.size();
}

//------------------------------------------------------------------------------
size_t Triangle::GetPointsInside(std::vector<double> & vec_x, std::vector<double> & vec_y)
{
	size_t size = data->GetSize();

	double x1 = vertex[0].GetX();
	double y1 = vertex[0].GetY();

	double x2 = vertex[1].GetX();
	double y2 = vertex[1].GetY();

	double x3 = vertex[2].GetX();
	double y3 = vertex[2].GetY();

	for(size_t num = 0; num < size; ++num) {
		Point2D p = data->GetPoint(num);
		double x = p.GetX();
		double y = p.GetY() ;

		double a = (x1 - x) * (y2 - y1) - (x2 - x1) * (y1 - y);
		double b = (x2 - x) * (y3 - y2) - (x3 - x2) * (y2 - y);
		double c = (x3 - x) * (y1 - y3) - (x1 - x3) * (y3 - y);

		if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0)) {
			vec_x.push_back(x);
			vec_y.push_back(y);
		}
	}
	return vec_x.size();
}
//------------------------------------------------------------------------------
void Triangle::Show()
{
	series->Visible=true;
}
//------------------------------------------------------------------------------
void Triangle::Hide()
{
	series->Visible=false;
}
//------------------------------------------------------------------------------
void Triangle::MakeSelection(bool ShowNeeded)
{
	series->LinePen->Width=5;
	if(ShowNeeded)
	{
		series->Visible = true;
	}
}
//------------------------------------------------------------------------------
void Triangle::RemoveSelection(bool HideNeeded)
{
	series->LinePen->Width=1;
	if(HideNeeded)
	{
		series->Visible = false;
	}
}
//------------------------------------------------------------------------------
void Triangle::GetRegionBoundariesX(double & left, double & right)
{
	double min = 32768;
	double max = -32768;

	for(size_t n = 0; n < vertex.size(); ++n) {
		double curr_x = vertex[n].GetX();
		if(curr_x < min) min = curr_x;
		if(curr_x > max) max = curr_x;
	}
	left = min;
	right = max;
}
//------------------------------------------------------------------------------
void Triangle::SetVertex(const double & x, const double & y)
{
//	if(++count > count_vertex) {
//
//	}
};
//------------------------------------------------------------------------------
// MultiRegion
//------------------------------------------------------------------------------
MultiRegion::MultiRegion(RawData * d, Graph * g, std::vector <Region*> Regions) :
	Region(d, g, "Union",UNION),_regions(Regions)
{

}
//------------------------------------------------------------------------------
MultiRegion::~MultiRegion()
{
}
//------------------------------------------------------------------------------
size_t MultiRegion::GetPointsInside(std::vector<Point2D> & points)
{
	std::vector<Point2D> tempPoints;
	size_t size = _regions.size();
	for(size_t i=0; i<size; i++)
	{
		_regions[i]->GetPointsInside(tempPoints);
		points.insert(points.end(), tempPoints.begin(), tempPoints.end());
	}

	size = points.size();
	for(size_t num = 0; num < size; ++num)
	{
		Point2D current_point = points.at(num);
		points.push_back(current_point);
	}
	return points.size();
}
//------------------------------------------------------------------------------
size_t MultiRegion::GetPointsInside(std::vector<double> & vec_x, std::vector<double> & vec_y)
{
	std::vector<Point2D> points;
	std::vector<Point2D> tempPoints;
	size_t size = _regions.size();
	for(size_t i=0; i<size; i++)
	{
		_regions[i]->GetPointsInside(tempPoints);
		points.insert(points.end(), tempPoints.begin(), tempPoints.end());
	}

	size = points.size();
	for(size_t num = 0; num < size; ++num)
	{
		Point2D current_point = points.at(num);

		double x = current_point.GetX();
		double y = current_point.GetY();

		vec_x.push_back(x);
		vec_y.push_back(y);
	}
	return vec_x.size();
}
//------------------------------------------------------------------------------
void MultiRegion::GetRegionBoundariesX(double & left, double & right)
{
	left = 0;
	right = 0;
}
//------------------------------------------------------------------------------
void MultiRegion::Show()
{
//todo
//	series->Visible=true;
}
//------------------------------------------------------------------------------
void MultiRegion::Hide()
{
//	series->Visible=false;
}
//------------------------------------------------------------------------------
/*!
 * \brief Highlight all regions in the multiregion
 */
void MultiRegion::MakeSelection(bool ShowNeeded)
{
	//todo:it doesn't work
//	for_each(_regions.begin(), _regions.end(), [](Region * r){r->series->LinePen->Width=5;}); 	//lambda doesn't fucking work with bcc32!!!
	if(_regions.size())
	{
		for(int i=0; i<=_regions.size()-1; i++)
		{
			_regions.at(i)->MakeSelection(ShowNeeded);
		}
	}

}
//------------------------------------------------------------------------------
/*!
 * \brief Remove the highlight all regions in the multiregion
 */
void MultiRegion::RemoveSelection(bool HideNeeded)
{
	//todo:it doesn't work
	if(_regions.size())
	{
		for(int i=0; i<=_regions.size()-1; i++)
		{
			_regions.at(i)->RemoveSelection(HideNeeded);
		}
	}
}
//------------------------------------------------------------------------------
// FreeForm
//------------------------------------------------------------------------------
FreeForm::FreeForm(RawData * d, Graph * g)
	: Region(d, g, "FreeForm",POLYGON), color(clGreen)
{
	const size_t point_size = 3;
	series = new TLineSeries(g->GetContext());
	series->ParentChart = g->GetContext();
	series->SeriesColor = color;
	series->XValues->Order = loNone;
	series->YValues->Order = loNone;

	series->Pointer->Visible = true;
	series->Pointer->Style = psCircle;
	series->Pointer->VertSize = point_size;
	series->Pointer->HorizSize = point_size;
}

//------------------------------------------------------------------------------
FreeForm::~FreeForm()
{
	delete series;
}

//------------------------------------------------------------------------------
void FreeForm::MouseDown(const int & x, const int & y)
{
	count++;

	const signed char ADigit = -3;
	double x1 = SimpleRoundTo(series->XScreenToValue(x), ADigit);
	double y1 = SimpleRoundTo(series->YScreenToValue(y), ADigit);
	vertex.push_back(Point2D(x1, y1));

	if(series->Count() == 0)
	{
		series->AddXY(x1, y1);
	}
	series->AddXY(x1, y1);
}

//------------------------------------------------------------------------------
void FreeForm::MouseMove(const int & x, const int & y)
{
	int curr_count = series->Count();
	if(curr_count != 0)
	{
		const signed char ADigit = -3;
		double x2 = SimpleRoundTo(series->XScreenToValue(x), ADigit);
		double y2 = SimpleRoundTo(series->YScreenToValue(y), ADigit);

		series->Delete(curr_count - 1);
		series->AddXY(x2, y2);

		if(count == count_vertex - 1)
		{
			double x = series->XValues->operator [](0);
			double y = series->YValues->operator [](0);
			series->AddXY(x, y);
		}

	}
}
//------------------------------------------------------------------------------
void FreeForm::KeyDown(WORD k)
{
	if(k == VK_RETURN)
	{
		count_vertex = series->Count()-1;
		series->Delete(series->Count() - 1);
		series->AddXY(vertex.front().x,vertex.front().y);
   		graph->NewRegionCreated(this);
	}
	else if (k == VK_ESCAPE)
	{
		graph->DeactivateRegion();
		series->Clear();
		count=0;
		vertex.clear();
    }
}
//------------------------------------------------------------------------------
size_t FreeForm::GetPointsInside(std::vector<Point2D> & points)
{
	size_t size = data->GetSize();

	for(size_t num = 0; num < size; ++num)
	{
		Point2D p = data->GetPoint(num);
		if (IsPointInPolygon(p))
		{
			points.push_back(p);
		}
	}
	return points.size();
}

//------------------------------------------------------------------------------
size_t FreeForm::GetPointsInside(std::vector<double> & vec_x, std::vector<double> & vec_y)
{
	size_t size = data->GetSize();

	for(size_t num = 0; num < size; ++num)
	{
		Point2D p = data->GetPoint(num);
		if (IsPointInPolygon(p))
		{
			vec_x.push_back(p.x);
			vec_y.push_back(p.y);
		}
	}
	return vec_x.size();
}
//------------------------------------------------------------------------------
bool FreeForm::IsPointInPolygon(Point2D p)
{
	bool pointIsInPolygon=false;
	if(count_vertex == 0)
	{
		return pointIsInPolygon;
    }
	if (count_vertex == 1)
	{
		pointIsInPolygon = (p == vertex.front());
		return pointIsInPolygon;
	}

	if (count_vertex == 2)
	{
		int c = p.Classify (vertex.front(),vertex.back());
		pointIsInPolygon = ( (c==Point2D::BETWEEN) || (c==Point2D::ORIGIN) || (c==Point2D::DESTINATION) );
		return pointIsInPolygon;
	}

	else
	{
		int i1, i2, S, S1, S2, S3;
		for (int n=0; n<count_vertex; n++)
		{
			pointIsInPolygon = false;
			i1 = n < count_vertex-1 ? n + 1 : 0;
			while (pointIsInPolygon == false)
			{
			   i2 = i1 + 1;
			   if (i2 >= count_vertex)
				i2 = 0;
			   if (i2 == (n < count_vertex-1 ? n + 1 : 0))
				 break;
			   S = abs ((int)vertex[i1].x * ((int)vertex[i2].y - (int)vertex[n ].y) +
						(int)vertex[i2].x * ((int)vertex[n ].y - (int)vertex[i1].y) +
						(int)vertex[n].x  * ((int)vertex[i1].y - (int)vertex[i2].y));

			   S1 = abs ((int)vertex[i1].x * ((int)vertex[i2].y - p.y) +
						 (int)vertex[i2].x * (p.y - (int)vertex[i1].y) +
						 p.x * ((int)vertex[i1].y - (int)vertex[i2].y));

			   S2 = abs ((int)vertex[n ].x * ((int)vertex[i2].y - p.y) +
						 (int)vertex[i2].x * (p.y - (int)vertex[n ].y) +
						 p.x * ((int)vertex[n ].y - (int)vertex[i2].y));

			   S3 = abs ((int)vertex[i1].x * ((int)vertex[n ].y - p.y) +
						 (int)vertex[n ].x * (p.y - (int)vertex[i1].y) +
						 p.x * ((int)vertex[i1].y - (int)vertex[n ].y));

			   if (S == S1 + S2 + S3)
			   {
					pointIsInPolygon = true;
					break;
			   }
			   i1 = i1 + 1;
			   if (i1 >= count_vertex)
				 i1 = 0;
			}
			if (pointIsInPolygon == false)
			break;
		}
		return pointIsInPolygon;
	}
}
//------------------------------------------------------------------------------
void FreeForm::Show()
{
	series->Visible=true;
}
//------------------------------------------------------------------------------
void FreeForm::Hide()
{
	series->Visible=false;
}
//------------------------------------------------------------------------------
void FreeForm::MakeSelection(bool ShowNeeded)
{
	series->LinePen->Width=5;
	if(ShowNeeded)
	{
		series->Visible = true;
	}
}
//------------------------------------------------------------------------------
void FreeForm::RemoveSelection(bool HideNeeded)
{
	series->LinePen->Width=1;
	if(HideNeeded)
	{
		series->Visible = false;
	}
}
//------------------------------------------------------------------------------
void FreeForm::GetRegionBoundariesX(double & left, double & right)
{
	double min = 32768;
	double max = -32768;

	for(size_t n = 0; n < vertex.size(); ++n)
	{
		double curr_x = vertex[n].GetX();
		if(curr_x < min) min = curr_x;
		if(curr_x > max) max = curr_x;
	}
	left = min;
	right = max;
}
//------------------------------------------------------------------------------
void FreeForm::SetVertex(const double & x, const double & y)
{
//	if(++count > count_vertex) {
//
//	}
};
#pragma package(smart_init)
