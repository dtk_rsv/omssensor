//---------------------------------------------------------------------------

#ifndef thrDataStreamH
#define thrDataStreamH
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
using namespace std;
//---------------------------------------------------------------------------
class TableCalculation;
class TableShape;
class Calibration;
typedef void CallbackFrame_t(vector<double>,vector<double>,vector<Point2D>,int);
typedef void CallbackVideo_t(TBitmap&);
typedef void CallbackException_t (Exception&);
class TDataStream:public TThread
{
	protected:
		void __fastcall Execute();
	public:
		enum THREADMODE {
			THREADMODE_MEASUREMENT	= 0,
			THREADMODE_SHOWHYSTOGRAM= 2,
			THREADMODE_SHOWPOINTS	= 3,
			THREADMODE_SHOWCIRCLES 	= 4
		};
		enum THREADPAR {
			TP_GRADIENT   = 0,
			TP_BUFFERSIZE = 1
		};
		enum FRAGMENTTYPE {
			FT_FULL = 0,
			FT_FIRST,
			FT_FRAGMENT
		};

		void Connect();
		void Disconnect();
		bool IsConnected();
		void Export();
		void SetMode(THREADMODE threadMode);
		void Send(NetCmd cmd);

		void Write(String Command, String Addr, String Mask, String Value);
		void Write(String Msg);
//		void Write(int Addr, int Mask, int Value);
		String Read(String Command);
//		int Read(int Addr);

		void SetParameter(THREADPAR Par, double value);

		__fastcall TDataStream (bool CreateSuspended,
								CallbackFrame_t callbackFrame,
								CallbackVideo_t callbackVideo,
								CallbackException_t callbackExc
							   );

		__fastcall ~TDataStream();

		Log * log;

	private:
		enum CONTROLPROTOCOL{CP_UDP, CP_TCP};
		enum CONNECTSTATE{CS_IDLE,CS_CONNECT,CS_DISCONNECT};
		enum {F_INT,F_DEC,F_GRAD};

	//private methods
		void _Connect();
		void _Disconnect();
		bool _Convert(double * x, double * y);
		void _ReceiveMeasurementString();
		void _ReceiveVideoFrame();
		void _SaveFrame();

	//callback methods
		void (*_callbackFrame)(vector<double>,vector<double>,vector<Point2D>,int);
		void (*_callbackVideo)(TBitmap&);
		void (*_callbackExc)(Exception&);

	//synchronize methods
		void __fastcall _UpdFrame();
		void __fastcall _ShowException();
		void __fastcall _UpdVideo();

   //private objects
		Calibration * _calibrationX;
		Calibration * _calibrationZ;
		queue<String> _commands;
		TStringList *_sl;
		TBitmap * _bmp;
		Exception * _exception;
		vector<double> _parameters;

	//data containers
		vector<double> _xValues;        //provided to increase the speed at
		vector<double> _yValues;        //updating visualization
		vector<Point2D> _xyValues;  	//provided to increase the speed at appending values to data
		unsigned int _threadMode;

	//flags
		bool _slAllowRec; 				//allow the thread to write data to string list
		bool _exportFrame;
		unsigned int _ctrlProtocol;    	//network protocol of device controlling (tcp or udp)
		unsigned int _connectState;
		unsigned int _parseMethod;		//incoming message format - integer or with decimal point
		bool _connected;
		FRAGMENTTYPE _fragmentType;
		unsigned int _count_points_received;

	//matrix size
		unsigned int _matrixWidth;
		unsigned int _matrixHeight;
		unsigned int _nOfBorders;

	//interframe filter
		unsigned int _filtrationRequired;
		unsigned int _filterSize;
		vector <vector<Filter*> > _filters;		//amount of filters is matrix width

   //minimal log buffer size
		unsigned int _minLogSize;
};
extern TDataStream *thrDataStream;
//---------------------------------------------------------------------------
#endif
